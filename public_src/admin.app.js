import React from 'react';
import ReactDOM from 'react-dom';

import {
  BrowserRouter as Router,
  HashRouter,
  Route,
  Redirect
} from 'react-router-dom';

import SectorList from './components/sector-list';
import ProductList from './components/product-list';
import ProductEditor from './components/product-editor';

ReactDOM.render((
	<HashRouter>
		<div>
			<Route exact path="/" render={() => (<Redirect to="/sectors" />)} />
			<Route exact path="/sectors" component={SectorList} />
			<Route exact path="/sectors/:sectorName/products" component={ProductList} />
			<Route exact path="/sectors/:sectorName/products/:productId/editor" component={ProductEditor} />
		</div>
	</HashRouter>
), document.getElementById('app'));