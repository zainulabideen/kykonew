import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import async from 'async';

import ServerApiHelper from '../helpers/server-api-helper';

import AccountNavigation from './account-navigation';

const styles = {
	container: {
		marginTop: 10
	},
};

class AccountProductList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: true,
			products: []
		};
	}

	onClickProduct(productId, event) {
		window.location.href = '#/accounts/' + this.props.match.params.accountToken + '/products/' + productId;
	}

	componentDidMount() {
		var _this = this;

		async.waterfall([
			function (callback) {
				ServerApiHelper.getAccountById(sessionStorage[_this.props.match.params.accountToken], callback);
			},

			function (account, callback) {
				ServerApiHelper.getProducts({ sector: account.type, status: 'publish' }, function (error, products) {
					callback(error, account, products);
				});
			}
		], function (error, account, products) {
			if (!error) {
				_this.setState({
					isLoading: false,
					products
				});
			} else {
				alert(error);
			}
		});
	}

	render() {
		var productListRender = this.state.products.map((product, index) => (
			<tr key={['product', product._id]}>
				<td style={{verticalAlign: 'middle', width: '1%', whiteSpace: 'nowrap', textAlign: 'right'}}>{index + 1}</td>
				<td>
					<button className="btn btn-link" style={{paddingLeft: 0}} onClick={this.onClickProduct.bind(this, product._id)}>{product.name}</button>
				</td>
			</tr>
		));

		return (
			<div className="container" style={styles.container}>
				<div className="row">
					<AccountNavigation accountToken={this.props.match.params.accountToken} />
				</div>
				<div className="row">
					<div className="col-md-12">
						<table className="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Product Name</th>
								</tr>
							</thead>
							<tbody>
								{(!this.state.isLoading) ? (
									productListRender
								) : (
									<tr>
										<td colSpan="2">Loading list. Please wait...</td>
									</tr>
								)}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(AccountProductList);