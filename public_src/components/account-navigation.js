import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import async from 'async';

import ServerApiHelper from '../helpers/server-api-helper';

const styles = {
	container: {
		marginBottom: 40
	},
};

class AccountNavigation extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			accountToken: props.accountToken
		};
	}

	onClickBack() {
		window.location.href = this.props.backUrl;
	}

	onClickLogout() {
		sessionStorage.removeItem(this.state.accountToken);
		window.location.href = '#/accounts/login';
	}

	componentDidMount() {
		if (!this.state.accountToken) {
			window.location.href = '#/accounts/login';
		}
	}

	render() {
		return (
			<div className="container-fluid" style={styles.container}>
				<div className="row">
					<div className="col-md-6" style={{textAlign: 'left'}}>
						{(this.props.backUrl) ? (
							<button className="btn btn-link" style={{paddingLeft: 0}} onClick={this.onClickBack.bind(this)}>
								<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span>BACK</span>
							</button>
						) : (
							<span>&nbsp;</span>
						)}
					</div>
					<div className="col-md-6" style={{textAlign: 'right'}}>
						<button className="btn btn-default" onClick={this.onClickLogout.bind(this)}>Logout</button>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(AccountNavigation);