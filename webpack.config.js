module.exports = {
    entry: {
        admin: __dirname + '/public_src/admin.app.js',
        main: __dirname + '/public_src/main.app.js'
    },
    output: {
        path: __dirname + '/public/js',
        filename: '[name].app.bundle.js',
    },
    module: {
        rules: [
            { 
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets:['react', 'env']
                }
            }
        ]
    }
};