import React from 'react';
import Radium from 'radium';

const styles = {
	container: {
		marginTop: 10
	},

	statementList: {
		width: '100%',
		paddingBottom: 5,
	},

	statementListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	statementListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	statementListColumnCount: {
		display: 'inline-block',
		width: '5%',
		textAlign: 'center'
	},

	statementListColumnText: {
		display: 'inline-block',
		width: '55%',
		textAlign: 'left'
	},

	statementListColumnFactor: {
		display: 'inline-block',
		width: '15%',
		textAlign: 'left'
	},

	statementListColumnEffect: {
		display: 'inline-block',
		width: '15%',
		textAlign: 'left'
	},

	statementListColumnAction: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},

	newStatementForm: {
		marginTop: 20,
		paddingTop: 10,
		paddingBottom: 10,
		borderBottom: '1px dashed #ccc'
	},
};

class ProductEditorStatements extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			factors: props.factors,
			statements: props.statements,
			newStatement: {
				text: '',
				factor: (props.factors.length > 0) ? props.factors[0].code : '',
				effect: 'positive'
			}
		};

		this.onChangeInput = this.onChangeInput.bind(this);
		this.onChangeInputCharacteristic = this.onChangeInputCharacteristic.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onChangeNewStatementInput = this.onChangeNewStatementInput.bind(this);
		this.onClickAddStatement = this.onClickAddStatement.bind(this);
		this.onClickDeleteStatement = this.onClickDeleteStatement.bind(this);
	}

	onChangeInput(idx, name, event) {
		var statements = this.state.statements;

		statements[idx][name] = event.target.value;

		this.setState({
			statements
		});
	}

	onChangeInputCharacteristic(idx, name, event) {
		var statements = this.state.statements;

		statements[idx].characteristic[name] = event.target.value;

		this.setState({
			statements
		});
	}

	onBlurInput() {
		var statements = this.state.statements;
		this.props.onChangeData('statements', statements);
	}

	onChangeNewStatementInput(name, event) {
		var newStatement = this.state.newStatement;

		newStatement[name] = event.target.value;

		this.setState({
			newStatement
		});
	}

	onClickAddStatement() {
		var statements = this.state.statements,
			newStatement = this.state.newStatement;

		statements.push({
			text: newStatement.text,
			characteristic: {
				_factor: newStatement.factor,
				effect: newStatement.effect,
				value: 1
			}
		});

		newStatement = {
			text: '',
			factor: (this.state.factors.length > 0) ? this.state.factors[0].code : '',
			effect: 'positive'
		};

		this.setState({
			newStatement
		});

		this.props.onChangeData('statements', statements);
	}

	onClickDeleteStatement(idx) {
		var confirm = prompt('Are you sure to delete this statement?\nType "yes" to continue.');

		if (confirm && confirm.toLowerCase() === 'yes') {
			var statements = this.state.statements;

			statements.splice(idx, 1);
			
			this.props.onChangeData('statements', statements);
		}
	}

	componentWillReceiveProps(props) {
		this.setState({
			factors: props.factors,
			statements: props.statements,
			newStatement: {
				text: '',
				factor: (props.factors.length > 0) ? props.factors[0].code : '',
				effect: 'positive'
			}
		});
	}

	render() {
		var factors = this.state.factors,
			statements = this.state.statements,
			newStatement = this.state.newStatement;

		var renderStatements = statements.map((item, index) => {
			var renderFactors = factors.map((fItem, fIndex) => (
				<option key={['statement', index, 'factor', fIndex].join('_')} value={fItem.code}>
					{fItem.code}
				</option>
			));

			return (
				<div key={['statement', index].join('_')} style={[styles.statementList, styles.statementListItem]}>
					<div style={styles.statementListColumnCount}>
						{index + 1}
					</div>
					<div style={styles.statementListColumnText}>
						<input 
							type="text" 
							placeholder="Statement text" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'text')} 
							onBlur={this.onBlurInput} 
							value={item.text}
						/>
					</div>
					<div style={styles.statementListColumnFactor}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeInputCharacteristic.bind(this, index, '_factor')} 
							onBlur={this.onBlurInput} 
							value={item.characteristic._factor} 
						>
							<option value=""></option>
							{renderFactors}
						</select>
					</div>
					<div style={styles.statementListColumnEffect}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeInputCharacteristic.bind(this, index, 'effect')} 
							onBlur={this.onBlurInput} 
							value={item.characteristic.effect} 
						>
							<option value="positive">positive</option>
							<option value="negative">negative</option>
						</select>
					</div>
					<div style={styles.statementListColumnAction}>
						<div 
							key={['statement', index, 'delete'].join('_')} 
							style={[styles.btnAction, styles.btnActionDelete]} 
							onClick={this.onClickDeleteStatement.bind(this, index)}
						>Delete</div>
					</div>
				</div>
			);
		});

		var renderFactors = factors.map((fItem, fIndex) => (
			<option key={['new', 'statement', 'factor', fIndex].join('_')} value={fItem.code}>
				{fItem.code}
			</option>
		));

		return (
			<div style={styles.container}>
				<div style={[styles.statementList, styles.statementListHeader]}>
					<div style={styles.statementListColumnCount}>#</div>
					<div style={styles.statementListColumnText}>Statement Text</div>
					<div style={styles.statementListColumnFactor}>Factor</div>
					<div style={styles.statementListColumnEffect}>Effect</div>
					<div style={styles.statementListColumnAction}>Action</div>
				</div>
				{(statements.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No statement has been add so far.</i>
					</div>
				) : null}
				{(statements.length > 0) ? (
					renderStatements
				) : null}
				<div style={styles.newStatementForm}>
					<div style={styles.statementListColumnCount}>
						#
					</div>
					<div style={styles.statementListColumnText}>
						<input 
							type="text" 
							placeholder="Statement text" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewStatementInput.bind(this, 'text')} 
							value={newStatement.text}
						/>
					</div>
					<div style={styles.statementListColumnFactor}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeNewStatementInput.bind(this, 'factor')} 
							value={newStatement.factor} 
						>
							{renderFactors}
						</select>
					</div>
					<div style={styles.statementListColumnEffect}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeNewStatementInput.bind(this, 'effect')} 
							value={newStatement.effect} 
						>
							<option value="positive">positive</option>
							<option value="negative">negative</option>
						</select>
					</div>
					<div style={styles.statementListColumnAction}>
						<div 
							key={['statement', 'add'].join('_')} 
							style={[styles.btnAction, styles.btnActionAdd]} 
							onClick={this.onClickAddStatement}
						>Add</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(ProductEditorStatements);