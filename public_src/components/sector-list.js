import React from 'react';
import Radium from 'radium';

const styles = {
	container: {
		display: 'flex',
		width: '100vw',
		height: '100vh',
		alignItems: 'center',
		justifyContent: 'center'
	},

	contentContainer: {
		display: 'inline-block'
	},

	sectorContainer: {
		display: 'inline-block',
		width: 160,
		padding: 10,
		marginRight: 10,
		border: '1px solid #ccc',
		borderRadius: 10,
		cursor: 'pointer',
		':hover': {
			border: '1px solid red',
			color: 'red'
		}
	},

	sectorImg: {
		width: 60,
		height: 60
	},

	sectorText: {
		fontSize: 24,
		fontWeight: 'bold'
	}
};

class SectorList extends React.Component {
	constructor(props) {
		super(props);

		this.onClickSector = this.onClickSector.bind(this);
	}

	onClickSector(sectorName) {
		this.props.history.push('/sectors/' + sectorName + '/products');
	}

	render() {
		return (
			<div style={styles.container}>
				<div style={styles.contentContainer}>
					<h1>Pick a Sector</h1>
					<div 
						key="btnPersonal" 
						style={styles.sectorContainer} 
						onClick={() => { this.onClickSector('personal'); }}
					>
						<img 
							style={styles.sectorImg} 
							src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAQAAACTbf5ZAAAAjElEQVR42u3PMQEAAAgDoK1/NUNZwV9oQDN5pcLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwlcL+VNgGQhK+A8AAAAASUVORK5CYII=" 
						/>
						<p style={styles.sectorText}>Personal</p>
					</div>
					<div 
						key="btnCorporate" 
						style={styles.sectorContainer} 
						onClick={() => { this.onClickSector('corporate'); }}
					>
						<img 
							style={styles.sectorImg} 
							src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAQAAACTbf5ZAAAAjElEQVR42u3PMQEAAAgDoK1/NUNZwV9oQDN5pcLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwlcL+VNgGQhK+A8AAAAASUVORK5CYII=" 
						/>
						<p style={styles.sectorText}>Corporate</p>
					</div>
					<div 
						key="btnEducation" 
						style={styles.sectorContainer} 
						onClick={() => { this.onClickSector('education'); }}
					>
						<img 
							style={styles.sectorImg} 
							src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAQAAACTbf5ZAAAAjElEQVR42u3PMQEAAAgDoK1/NUNZwV9oQDN5pcLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwlcL+VNgGQhK+A8AAAAASUVORK5CYII=" 
						/>
						<p style={styles.sectorText}>Education</p>
					</div>
				</div>	
			</div>
		);
	}
}

module.exports = Radium(SectorList);