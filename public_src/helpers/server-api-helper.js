import _ from 'underscore';
import async from 'async';

export default class ServerApiHelper {
	static getProducts(filter, callback) {
		var _params = (filter) ? ('?_params=' + JSON.stringify({
			where: filter
		})) : '';

		$.ajax({
            type: 'GET',
            url: '/core/collections/products' + _params,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                	callback(null, _.sortBy(result.data, 'createAt'));
                } else {
                	callback('Unable to retrieve products');
                }
            },
            error: function () {
                callback('Unable to retrieve products');
            }
        });
	}

	static getProductById(id, callback) {
		$.ajax({
            type: 'GET',
            url: '/core/collections/products/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                	callback(null, result.data);
                } else {
                	callback('Unable to find product by given id.');
                }
            },
            error: function () {
                callback('Unable to find product by given id.');
            }
        });
	}

	static addProduct(data, callback) {
		$.ajax({
            type: 'POST',
            url: '/core/collections/products',
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data,
            success: function (result) {
                if (result && result.success) {
                	callback(null, result.data);
                } else {
                	console.error(result.error);
                	callback('Unable to add new product.');
                }
            },
            error: function () {
                callback('Unable to add new product.');
            }
        });
	}

	static updateProduct(id, data, callback) {
		$.ajax({
            type: 'PUT',
            url: '/core/collections/products/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data,
            success: function (result) {
                if (result && result.success) {
                	callback(null, result.data);
                } else {
                	callback('Unable to update product.');
                }
            },
            error: function () {
                callback('Unable to update product.');
            }
        });
	}

	static duplicateProduct(id, callback) {
		async.waterfall([
			function (callback) {
				$.ajax({
		            type: 'GET',
		            url: '/core/collections/products/' + id,
		            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
		            dataType: 'json',
		            success: function (result) {
		                if (result && result.success) {
		                	callback(null, result.data);
		                } else {
		                	callback('Unable to find product by given id.');
		                }
		            },
		            error: function () {
		                callback('Unable to find product by given id.');
		            }
		        });
			},

			function (product, callback) {
				var newProduct = _.omit(product, ['_id', 'createdAt', 'updatedAt', 'status']);
				newProduct.name += ' (DUPLICATE)';
                newProduct.status = 'draft';
				callback(null, newProduct);
			},

			function (newProduct, callback) {
				$.ajax({
		            type: 'POST',
		            url: '/core/collections/products',
		            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
		            dataType: 'json',
		            data: newProduct,
		            success: function (result) {
		                if (result && result.success) {
		                	callback(null, result.data);
		                } else {
		                	console.error(result.error);
		                	callback('Unable to add new product.');
		                }
		            },
		            error: function () {
		                callback('Unable to add new product.');
		            }
		        });
			}
		], function (error, product) {
			callback(error, product);
		});
	}

	static deleteProduct(id, callback) {
		$.ajax({
            type: 'DELETE',
            url: '/core/collections/products/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                	callback(null, result.data);
                } else {
                	callback('Unable to delete product by given id.');
                }
            },
            error: function () {
                callback('Unable to delete product by given id.');
            }
        });
	}

    static awardProductExamination(accountId, productId, totalExamination, callback) {
        async.waterfall([
            function (callback) {
                $.ajax({
                    type: 'GET',
                    url: '/core/collections/accounts/' + accountId,
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, result.data);
                        } else {
                            callback('Unable to find account by given id.');
                        }
                    },
                    error: function () {
                        callback('Unable to find account by given id.');
                    }
                });
            },

            function (account, callback) {
                var availableExaminations = account.availableExaminations,
                    existIdx = _.findIndex(availableExaminations, { _product: productId });

                if (existIdx !== -1) {
                    availableExaminations[existIdx].total += totalExamination;
                } else {
                    availableExaminations.push({
                        _product: productId,
                        total: totalExamination
                    });
                }

                callback(null, account, availableExaminations);
            },

            function (account, availableExaminations, callback) {
                $.ajax({
                    type: 'PUT',
                    url: '/core/collections/accounts/' + account._id,
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    data: {
                        availableExaminations
                    },
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, result.data);
                        } else {
                            callback('Unable to update account.');
                        }
                    },
                    error: function () {
                        callback('Unable to update account.');
                    }
                });
            }
        ], function (error, account) {
            callback(error, account);
        });
    }

    static purchaseProductExamination(accountId, productId, totalExamination, totalAmount, payment, callback) {
        async.waterfall([
            function (callback) {
                $.ajax({
                    type: 'GET',
                    url: '/core/collections/accounts/' + accountId,
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, result.data);
                        } else {
                            callback('Unable to find account by given id.');
                        }
                    },
                    error: function () {
                        callback('Unable to find account by given id.');
                    }
                });
            },

            function (account, callback) {
                var availableExaminations = account.availableExaminations,
                    existIdx = _.findIndex(availableExaminations, { _product: productId });

                if (existIdx !== -1) {
                    availableExaminations[existIdx].total += totalExamination;
                } else {
                    availableExaminations.push({
                        _product: productId,
                        total: totalExamination
                    });
                }

                callback(null, account, availableExaminations);
            },

            function (account, availableExaminations, callback) {
                var purchaseHistories = account.purchaseHistories;

                purchaseHistories.push({
                    _product: productId,
                    totalExamination,
                    totalAmount,
                    transactionInfo: payment
                });

                callback(null, account, availableExaminations, purchaseHistories);
            },

            function (account, availableExaminations, purchaseHistories, callback) {
                $.ajax({
                    type: 'PUT',
                    url: '/core/collections/accounts/' + account._id,
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    data: {
                        availableExaminations,
                        purchaseHistories
                    },
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, result.data);
                        } else {
                            callback('Unable to update account.');
                        }
                    },
                    error: function () {
                        callback('Unable to update account.');
                    }
                });
            }
        ], function (error, account) {
            callback(error, account);
        });
    }

    static getExaminations(filter, callback) {
        var _params = (filter) ? ('?_params=' + JSON.stringify({
            where: filter
        })) : '';

        $.ajax({
            type: 'GET',
            url: '/core/collections/examinations' + _params,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                    callback(null, _.sortBy(result.data, 'createAt'));
                } else {
                    callback('Unable to retrieve examinations');
                }
            },
            error: function () {
                callback('Unable to retrieve examinations');
            }
        });
    }

    static getExaminationById(id, callback) {
        $.ajax({
            type: 'GET',
            url: '/core/collections/examinations/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                    callback(null, result.data);
                } else {
                    callback('Unable to find examination by given id.');
                }
            },
            error: function () {
                callback('Unable to find examination by given id.');
            }
        });
    }

    static addExamination(data, callback) {
        $.ajax({
            type: 'POST',
            url: '/core/collections/examinations',
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data,
            success: function (result) {
                if (result && result.success) {
                    callback(null, result.data);
                } else {
                    console.error(result.error);
                    callback('Unable to add new examination.');
                }
            },
            error: function () {
                callback('Unable to add new examination.');
            }
        });
    }

    static updateExamination(id, data, callback) {
        $.ajax({
            type: 'PUT',
            url: '/core/collections/examinations/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data,
            success: function (result) {
                if (result && result.success) {
                    callback(null, result.data);
                } else {
                    callback('Unable to update examination.');
                }
            },
            error: function () {
                callback('Unable to update examination.');
            }
        });
    }

    static deleteExamination(id, callback) {
        $.ajax({
            type: 'DELETE',
            url: '/core/collections/examinations/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                    callback(null, result.data);
                } else {
                    callback('Unable to delete examination by given id.');
                }
            },
            error: function () {
                callback('Unable to delete examination by given id.');
            }
        });
    }

    static examinationInvitation(accountId, productId, email, additionalInformation, callback) {
        async.waterfall([
            function (callback) {
                $.ajax({
                    type: 'GET',
                    url: '/core/collections/accounts/' + accountId,
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, result.data);
                        } else {
                            callback('Unable to find account by given id.');
                        }
                    },
                    error: function () {
                        callback('Unable to find account by given id.');
                    }
                });
            },

            function (account, callback) {
                var availableExaminations = account.availableExaminations,
                    existIdx = _.findIndex(availableExaminations, { _product: productId });

                if (existIdx !== -1) {
                    availableExaminations[existIdx].total -= 1;

                    if (availableExaminations[existIdx].total === 0) {
                        availableExaminations.splice(existIdx, 1);
                    }

                    callback(null, account, availableExaminations);
                } else {
                    callback('Unable to find existing examination credit.');
                }
            },

            function (account, availableExaminations, callback) {
                $.ajax({
                    type: 'POST',
                    url: '/core/collections/examinations',
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    data: {
                        _account: accountId,
                        _product: productId,
                        email,
                        additionalInformation
                    },
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, account, availableExaminations, result.data);
                        } else {
                            callback(result.error);
                        }
                    },
                    error: function () {
                        callback('Internal error.');
                    }
                });
            },

            function (account, availableExaminations, examination, callback) {
                $.ajax({
                    type: 'PUT',
                    url: '/core/collections/accounts/' + account._id,
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    data: {
                        availableExaminations
                    },
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, result.data, examination);
                        } else {
                            callback('Unable to update account.');
                        }
                    },
                    error: function () {
                        callback('Unable to update account.');
                    }
                });
            },

            function (account, examination, callback) {
                var examinationLink = window.location.protocol + "//" + window.location.host + '/app#/products/' + productId + '/examinations/' + examination._id;

                $.ajax({
                    type: 'POST',
                    url: '/core/emails',
                    beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
                    dataType: 'json',
                    data: {
                        to: email,
                        content: {
                            subject: 'Kyko Examination Invitation.',
                            html: [
                                'You have been invited to participate in Kyko examination.',
                                '<br/><br/>',
                                'Click on following link to proceed, <a href="' + examinationLink + '">TAKE EXAMINATION</a>'
                            ].join(''),
                            text: [
                                'You have been invited to participate in Kyko examination.',
                                '\n\n',
                                'Click on following link to proceed, ' + examinationLink,
                            ].join('')
                        }
                    },
                    success: function (result) {
                        if (result && result.success) {
                            callback(null, examination);
                        } else {
                            callback(result.error);
                        }
                    },
                    error: function () {
                        callback('Internal error.');
                    }
                });
            }
        ], function (error, examination) {
            callback(error, examination)
        });
    }

    static login(username, password, callback) {
        $.ajax({
            type: 'POST',
            url: '/api/v1/login',
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data: {
                username,
                password
            },
            success: function (result) {
                if (result && result.success) {
                    callback(null, result.data);
                } else {
                    callback(result.error);
                }
            },
            error: function () {
                callback('Invalid username or password.');
            }
        });
    }

    static passwordReset(username, callback) {
        $.ajax({
            type: 'POST',
            url: '/api/v1/passwordreset',
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data: {
                username
            },
            success: function (result) {
                if (result && result.success) {
                    callback();
                } else {
                    callback(result.error);
                }
            },
            error: function () {
                callback('Invalid username.');
            }
        });
    }

    static register(data, callback) {
        $.ajax({
            type: 'POST',
            url: '/api/v1/register',
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            data,
            success: function (result) {
                if (result && result.success) {
                    callback();
                } else {
                    callback(result.error);
                }
            },
            error: function () {
                callback('Internal error.');
            }
        });
    }

    static getAccountById(id, callback) {
        $.ajax({
            type: 'GET',
            url: '/core/collections/accounts/' + id,
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success) {
                    callback(null, result.data);
                } else {
                    callback('Unable to find account by given id.');
                }
            },
            error: function () {
                callback('Unable to find account by given id.');
            }
        });
    }

    static getAccountByUsername(username, callback) {
        var _params = {
            where: {
                username
            }
        };

        $.ajax({
            type: 'GET',
            url: '/core/collections/accounts?_params=' + JSON.stringify(_params),
            beforeSend: function(xhr){ xhr.setRequestHeader('x-meijinjs-api-key', 'kyk0v2ap1k3y'); },
            dataType: 'json',
            success: function (result) {
                if (result && result.success && result.data.length > 0) {
                    callback(null, result.data[0]);
                } else {
                    callback('Unable to find account by given username.');
                }
            },
            error: function () {
                callback('Unable to find account by given username.');
            }
        });
    }
}