import React from 'react';
import Radium from 'radium';

const styles = {
	container: {
		marginTop: 10
	},

	factorList: {
		width: '100%',
		paddingBottom: 5,
	},

	factorListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	factorListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	factorListColumnCode: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'left'
	},

	factorListColumnName: {
		display: 'inline-block',
		width: '20%',
		textAlign: 'left'
	},

	factorListColumnDescription: {
		display: 'inline-block',
		width: '60%',
		textAlign: 'left'
	},

	factorListColumnAction: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},

	newFactorForm: {
		marginTop: 20,
		paddingTop: 10,
		paddingBottom: 10,
		borderBottom: '1px dashed #ccc'
	},
};

class ProductEditorFactors extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			factors: props.factors,
			newFactor: {
				code: '',
				name: '',
				description: ''
			}
		};

		this.onChangeInput = this.onChangeInput.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onClickDeleteFactor = this.onClickDeleteFactor.bind(this);
		this.onChangeNewFactorInput = this.onChangeNewFactorInput.bind(this);
		this.onClickAddFactor = this.onClickAddFactor.bind(this);
	}

	onChangeInput(idx, name, event) {
		var factors = this.state.factors;

		factors[idx][name] = event.target.value;

		if (name === 'code') {
			factors[idx].code = factors[idx].code.toUpperCase();
		}

		this.setState({
			factors
		});
	}

	onBlurInput() {
		var factors = this.state.factors;
		this.props.onChangeData('factors', factors);
	}

	onClickDeleteFactor(index) {
		var confirm = prompt('Are you sure to delete this factor?\nType "yes" to continue.');

		if (confirm && confirm.toLowerCase() === 'yes') {
			var factors = this.state.factors;

			factors.splice(index, 1);
			
			this.props.onChangeData('factors', factors);
		}
	}

	onChangeNewFactorInput(name, event) {
		var newFactor = this.state.newFactor;

		newFactor[name] = event.target.value;

		if (name === 'code') {
			newFactor.code = newFactor.code.toUpperCase();
		}

		this.setState({
			newFactor
		});
	}

	onClickAddFactor() {
		var factors = this.state.factors,
			newFactor = this.state.newFactor;

		factors.push({
			code: newFactor.code.trim(),
			name: newFactor.name.trim(),
			description: newFactor.description.trim()
		});

		newFactor = {
			code: '',
			name: '',
			description: ''
		};

		this.setState({
			newFactor
		});

		this.props.onChangeData('factors', factors);
	}

	componentWillReceiveProps(props) {
		this.setState({
			factors: props.factors
		});
	}

	render () {
		var factors = this.state.factors;
		var renderFactors = factors.map((item, index) => (
			<div key={'factor_' + index} style={[styles.factorList, styles.factorListItem]}>
				<div style={styles.factorListColumnCode}>
					<input 
						type="text" 
						placeholder="Factor Code" 
						style={{width: '90%'}} 
						onChange={this.onChangeInput.bind(this, index, 'code')} 
						onBlur={this.onBlurInput} 
						value={item.code}
					/>
				</div>
				<div style={styles.factorListColumnName}>
					<input 
						type="text" 
						placeholder="Name" 
						style={{width: '90%'}} 
						onChange={this.onChangeInput.bind(this, index, 'name')} 
						onBlur={this.onBlurInput} 
						value={item.name}
					/>
				</div>
				<div style={styles.factorListColumnDescription}>
					<input 
						type="text" 
						placeholder="Description" 
						style={{width: '90%'}} 
						onChange={this.onChangeInput.bind(this, index, 'description')} 
						onBlur={this.onBlurInput} 
						value={item.description}
					/>
				</div>
				<div style={styles.factorListColumnAction}>
					<div 
						key={['factor', index, 'delete'].join('_')} 
						style={[styles.btnAction, styles.btnActionDelete]} 
						onClick={this.onClickDeleteFactor.bind(this, index)}
					>Delete</div>
				</div>
			</div>
		));

		return (
			<div style={styles.container}>
				<div style={[styles.factorList, styles.factorListHeader]}>
					<div style={styles.factorListColumnCode}>Factor Code</div>
					<div style={styles.factorListColumnName}>Name</div>
					<div style={styles.factorListColumnDescription}>Description</div>
					<div style={styles.factorListColumnAction}>Action</div>
				</div>
				{(factors.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No factor has been add so far.</i>
					</div>
				) : null}
				{(factors.length > 0) ? (
					renderFactors
				) : null}
				<div style={styles.newFactorForm}>
					<div style={styles.factorListColumnCode}>
						<input 
							type="text" 
							placeholder="Factor Code" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewFactorInput.bind(this, 'code')} 
							value={this.state.newFactor.code}
						/>
					</div>
					<div style={styles.factorListColumnName}>
						<input 
							type="text" 
							placeholder="Name" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewFactorInput.bind(this, 'name')} 
							value={this.state.newFactor.name}
						/>
					</div>
					<div style={styles.factorListColumnDescription}>
						<input 
							type="text" 
							placeholder="Description" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewFactorInput.bind(this, 'description')} 
							value={this.state.newFactor.description}
						/>
					</div>
					<div style={styles.factorListColumnAction}>
						<div key={'factor_add'} style={[styles.btnAction, styles.btnActionAdd]} onClick={this.onClickAddFactor}>
							Add
						</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(ProductEditorFactors);