import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import async from 'async';
import Shuffle from 'knuth-shuffle';

import ServerApiHelper from '../helpers/server-api-helper';

const styles = {
	container: {
		marginTop: 10,
		textAlign: 'center'
	},

	instructionContainer: {
		marginTop: 20,
		width: '80%',
		display: 'inline-block',
		textAlign: 'justify'
	},

	instructionIntensityContainer: {
		marginBottom: 20,
		paddingLeft: 40
	},

	questionContainer: {
		display: 'inline-flex',
		width: '80%',
		border: '1px solid #ccc'
	},

	questionAnswered: {
		backgroundColor: '#DCDCDC'
	},

	questionNumber: {
		display: 'inline-block',
		width: 40,
		textAlign: 'right',
		padding: 5,
		borderRight: '1px solid #ccc'
	},

	questionText: {
		display: 'inline-block',
		flexGrow: 1,
		textAlign: 'justify',
		padding: 5,
	},

	questionAnswers: {
		display: 'inline-block',
		textAlign: 'center',
		padding: 5,
		paddingLeft: 15,
		borderLeft: '1px solid #ccc'
	},

	answerOption: {
		display: 'inline-block',
		marginRight: 15
	}
};

class ProductExamination extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			productId: this.props.match.params.productId,
			examinationId: this.props.match.params.examinationId,
			product: null,
			questions: [],
			answers: []
		}
	}

	onClickAnswer(questionId, factor, effect, effectValue, answerValue, event) {
		var answers = this.state.answers;

		var idx = _.findIndex(answers, { _statement: questionId });

		if (idx !== -1) {
			answers[idx].value = answerValue
		} else {
			answers.push({
				_statement: questionId,
				value: answerValue
			});
		}

		var result = {
			factors: [],
			dimensions: [],
			variables: []
		};

		var i = 0;

		for (i = 0; i < this.state.product.factors.length; i++) {
			var fCode = this.state.product.factors[i].code;
			var fQuestions = _.filter(this.state.product.statements, (item) => {
				return (item.characteristic._factor === fCode);
			});
			var fQuestionIds = _.pluck(fQuestions, '_id');
			var fAnswers = _.filter(answers, (item) => {
				return (fQuestionIds.indexOf(item._statement) !== -1);
			});

			var score = 0;

			for (var j = 0; j < fAnswers.length; j++) {
				var q = _.findWhere(fQuestions, { _id: fAnswers[j]._statement });
				var effect = q.characteristic.effect;
				var effectValue = q.characteristic.value;

				switch (effect) {
					case 'positive':
						score += (fAnswers[j].value * effectValue);
						break;
					case 'negative':
						score += ((7 - fAnswers[j].value + 1) * effectValue);
						break;
				}
			}

			if (score > 0) {
				score /= fAnswers.length;
			}

			result.factors.push({
				code: fCode,
				score
			});
		}

		for (i = 0; i < this.state.product.dimensions.length; i++) {
			var d = this.state.product.dimensions[i];

			var score = 0;

			for (var j =0; j < d.effects.length; j++) {
				var fScore = _.findWhere(result.factors, { code: d.effects[j]._factor });
				var effect = d.effects[j].effect;
				var effectValue = d.effects[j].value;

				if (fScore !== undefined) {
					switch (effect) {
						case 'positive':
							score += (fScore.score * effectValue);
							break;
						case 'negative':
							score += ((7 - fScore.score + 1) * effectValue);
							break;
					}
				}
			}

			if (score > 0) {
				score /= d._factors.length;
			}

			result.dimensions.push({
				code: d.code,
				score
			});
		}

		for (i = 0; i < this.state.product.variables.length; i++) {
			var v = this.state.product.variables[i];

			var score = 0;

			for (var j =0; j < v.effects.length; j++) {
				var fScore = _.findWhere(result.factors, { code: v.effects[j]._factor });
				var effect = v.effects[j].effect;
				var effectValue = v.effects[j].value;

				if (fScore !== undefined) {
					switch (effect) {
						case 'positive':
							score += (fScore.score * effectValue);
							break;
						case 'negative':
							score += ((7 - fScore.score + 1) * effectValue);
							break;
					}
				}
			}

			if (score > 0) {
				score /= v._factors.length;
			}

			result.variables.push({
				code: v.name,
				score
			});
		}

		var data = {
			answers,
			result,
			isCompleted: (answers.length === this.state.questions.length) ? true : false
		};

		this.setState({
			answers
		}, () => {
			ServerApiHelper.updateExamination(this.state.examinationId, data, (error, examination) => {
				// do nothing
			});
		});
	}

	componentDidMount() {
		var _this = this;

		async.waterfall([
			(callback) => {
				ServerApiHelper.getExaminationById(_this.state.examinationId, callback);
			},

			(examination, callback) => {
				if (examination._product === _this.state.productId) {
					ServerApiHelper.getProductById(_this.state.productId, (error, product) => {
						if (!error) {
							callback(null, examination, product);
						} else {
							callback(error);
						}
					});
				} else {
					callback('Invalid examination. Please make sure to using correct URL.');
				}
			},

			(examination, product, callback) => {
				if (product.statementsRandomOrder.length !== product.statements.length) {
					var statementIds = _.pluck(product.statements, '_id'),
						shuffleStamentIds = Shuffle.knuthShuffle(statementIds);

					product.statementsRandomOrder = shuffleStamentIds;

					var data = product;

					_.omit(data, ['_id', 'createdAt', 'updatedAt']);

					ServerApiHelper.updateProduct(product._id, data, (error, product) => {
						callback(error, examination, product);
					});
				} else {
					callback(null, examination, product);
				}
			},

			(examination, product, callback) => {
				// In case effect of factor never been assigned.
				var i = 0;

				for (i = 0; i < product.dimensions.length; i++) {
					var factors = product.dimensions[i]._factors,
						effects = product.dimensions[i].effects;

					if (factors.length > 0 && effects.length === 0) {
						for (var j = 0; j < factors.length; j++) {
							product.dimensions[i].effects.push({
								_factor: factors[j],
								effect: 'positive',
								value: 1
							});
						}
					}
				}

				for (i = 0; i < product.variables.length; i++) {
					var factors = product.variables[i]._factors,
						effects = product.variables[i].effects;

					if (factors.length > 0 && effects.length === 0) {
						for (var j = 0; j < factors.length; j++) {
							product.variables[i].effects.push({
								_factor: factors[j],
								effect: 'positive',
								value: 1
							});
						}
					}
				}

				callback(null, examination, product);
			}
		], (error, examination, product) => {
			if (!error) {
				_this.setState({
					product,
					questions: product.statementsRandomOrder,
					answers: examination.answers
				});
			} else {
				alert(error);
			}
		});
	}

	render() {
		var renderQuestions = this.state.questions.map((questionId, index) => {
			var question = null;

			if (questionId !== undefined && questionId.length > 0) {
				question = _.findWhere(this.state.product.statements, { _id: questionId });
			} else {
				question = this.state.product.statements[this.state.product.statements.length - 1];
			}

			var answer = _.findWhere(this.state.answers, { _statement: question._id });

			var renderAnswerOptions = [...Array(7)].map((item, oIndex) => {
				return (
					<div key={['question', index, 'answer', oIndex].join('_')} style={styles.answerOption}>
						<input
							type="radio"
							name={[question._id, 'answer'].join('_')}
							value={oIndex + 1}
							defaultChecked={(answer && answer.value === (oIndex + 1)) ? true : false}
							onClick={this.onClickAnswer.bind(this, question._id, question.characteristic._factor, question.characteristic.effect, question.characteristic.value, (oIndex + 1))}
						/>
						<span>&nbsp;{oIndex + 1}</span>
					</div>
				);
			});

			return (
				<div key={['question', index].join('_')} style={[styles.questionContainer, (answer) ? styles.questionAnswered : null]}>
					<div style={styles.questionNumber}>
						{index + 1}
					</div>
					<div style={styles.questionText}>
						{question.text}
					</div>
					<div style={styles.questionAnswers}>
						{renderAnswerOptions}
					</div>
				</div>
			);
		});
		return (
			<div style={styles.container}>
				<h1>Welcome to {(this.state.product) ? this.state.product.name : null}</h1>
				<div style={styles.instructionContainer}>
					This instrument is intended to measure various dimensions of human
					characteristics that will help you understand reasons for your own
					preferences. There are <u><b>no right or wrong answers</b></u>.
					Be sincere and honest in your response to each statement for a more
					accurate profile of yourself.
				</div>
				<div style={styles.instructionContainer}>
					Use the scale shown below to indicate the intensity that each statement
					describes your <u><b>current status</b></u> (what you are now) and NOT what <u><b>you were
					in the past</b></u> or what you <u><b>would like to be</b></u> in the future.
				</div>
				<div style={[styles.instructionContainer, styles.instructionIntensityContainer]}>
					7 - A lot like me/Strongly agree
					<br/>
					6 - Like me/Agree
					<br/>
					5 - Quite like me/Quite agree
					<br/>
					4 - Neutral
					<br/>
					3 - Seldom like me/Quite disagree
					<br/>
					2 - Least like me/Disagree
					<br/>
					1 - Not like me/Disagree strongly
					<br/>
				</div>
				{renderQuestions}
			</div>

		);
	}
}

module.exports = Radium(ProductExamination);
