import React from 'react';
import Radium from 'radium';
import _ from 'underscore';

const styles = {
	container: {
		marginTop: 10
	},

	variableList: {
		width: '100%',
		paddingBottom: 5,
	},

	variableListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	variableListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	variableListColumnName: {
		display: 'inline-block',
		width: '13%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	variableListColumnDescription: {
		display: 'inline-block',
		width: '25%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	variableListColumnType: {
		display: 'inline-block',
		width: '12%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	variableListColumnFactors: {
		display: 'inline-block',
		width: '10%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	variableListColumnInterpretation: {
		display: 'inline-block',
		width: '32%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	variableListColumnAction: {
		display: 'inline-block',
		width: '8%',
		verticalAlign: 'top',
		textAlign: 'center'
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},

	newVariableForm: {
		marginTop: 20,
		paddingTop: 10,
		paddingBottom: 10,
		borderBottom: '1px dashed #ccc'
	},
};

class ProductEditorVariables extends React.Component {
	constructor(props) {
		super(props);

		// to handle previous variable that not have effect assign
		var variables = props.variables;

		for (var i = 0; i < variables.length; i++) {
			var factors = variables[i]._factors,
				effects = variables[i].effects;

			if (factors.length > 0 && effects.length === 0) {
				for (var j = 0; j < factors.length; j++) {
					variables[i].effects.push({
						_factor: factors[j],
						effect: 'positive',
						value: 1
					});
				}
			}
		}
		// END

		this.state = {
			factors: props.factors,
			variables,
			newVariable: {
				name: '',
				description: '',
				type: 'competency',
				_factors: [],
				interpretation: {
					low: '',
					fLow: '',
					average: '',
					fHigh: '',
					high: ''
				}
			}
		};

		this.onChangeInput = this.onChangeInput.bind(this);
		this.onChangeInputFactors = this.onChangeInputFactors.bind(this);
		this.onChangeInputInterpretation = this.onChangeInputInterpretation.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onClickDeleteVariable = this.onClickDeleteVariable.bind(this);
		this.onChangeNewVariableInput = this.onChangeNewVariableInput.bind(this);
		this.onClickAddVariable = this.onClickAddVariable.bind(this);
	}

	onChangeInput(idx, name, event) {
		var variables = this.state.variables;

		variables[idx][name] = event.target.value;

		this.setState({
			variables
		});
	}

	onChangeInputFactors(idx, factorCode, event) {
		var variables = this.state.variables;

		if (event.target.checked) {
			if (variables[idx]._factors.indexOf(factorCode) === -1) {
				variables[idx]._factors.push(factorCode);

				variables[idx].effects.push({
					_factor: factorCode,
					effect: 'positive',
					value: 1
				});
			}
		} else {
			var fCodeIdx = variables[idx]._factors.indexOf(factorCode);
			if (fCodeIdx >= 0) {
				variables[idx]._factors.splice(fCodeIdx, 1);
				variables[idx].effects.splice(fCodeIdx, 1);
			}
		}

		// this.setState({
		// 	variables
		// });

		this.props.onChangeData('variables', variables);
	}

	onChangeFactorEffect(idx, factorCode, event) {
		var variables = this.state.variables;

		var $curr = $(event.target),
			effectVal = ($curr.text() === '+') ? 'negative' : 'positive',
			fCodeIdx = variables[idx]._factors.indexOf(factorCode);

		if (fCodeIdx !== -1) {
			var factorEffectIdx = _.findIndex(variables[idx].effects, { _factor: factorCode });

			if (factorEffectIdx !== -1) {
				variables[idx].effects[factorEffectIdx].effect = effectVal;
				this.props.onChangeData('variables', variables);
			}
		}
	}

	onChangeInputInterpretation(idx, levelName, event) {
		var variables = this.state.variables;

		variables[idx].interpretation[levelName] = event.target.value;

		this.setState({
			variables
		});
	}

	onBlurInput() {
		var variables = this.state.variables;
		this.props.onChangeData('variables', variables);
	}

	onClickDeleteVariable(idx) {
		var confirm = prompt('Are you sure to delete this variable?\nType "yes" to continue.');

		if (confirm && confirm.toLowerCase() === 'yes') {
			var variables = this.state.variables;

			variables.splice(idx, 1);
			
			this.props.onChangeData('variables', variables);
		}
	}

	onChangeNewVariableInput(name, event) {
		var newVariable = this.state.newVariable;

		newVariable[name] = event.target.value;

		this.setState({
			newVariable
		});
	}

	onClickAddVariable() {
		var variables = this.state.variables,
			newVariable = this.state.newVariable;

		variables.push({
			name: newVariable.name.trim(),
			description: newVariable.description.trim(),
			type: newVariable.type.trim(),
			_factors: [],
			interpretation: {
				low: '',
				fLow: '',
				average: '',
				fHigh: '',
				high: ''
			}
		});

		newVariable = {
			name: '',
			description: '',
			type: 'competency',
			_factors: [],
			interpretation: {
				low: '',
				fLow: '',
				average: '',
				fHigh: '',
				high: ''
			}
		};

		this.setState({
			newVariable
		});

		this.props.onChangeData('variables', variables);
	}

	componentWillReceiveProps(props) {
		this.setState({
			factors: props.factors,
			variables: props.variables
		});
	}

	render () {
		var factors = this.state.factors;
		var variables = this.state.variables;
		var newVariable = this.state.newVariable;

		var renderVariables = variables.map((item, index) => {
			var renderFactors = factors.map((fItem, fIndex) => (
				<div key={['variable', index, 'factor', fIndex].join('_')}>
					<input 
						type="checkbox" 
						onChange={this.onChangeInputFactors.bind(this, index, fItem.code)} 
						checked={(item._factors.indexOf(fItem.code) >= 0) ? true : false} 
					/>&nbsp;{fItem.code}&nbsp;
					<button 
						style={{width: '22px', height: '22px'}} 
						className={(item._factors.indexOf(fItem.code) >= 0 && _.findWhere(item.effects, { _factor: fItem.code }).effect === 'negative') ? 'btn btn-xs btn-danger' : 'btn btn-xs btn-success'} 
						onClick={this.onChangeFactorEffect.bind(this, index, fItem.code)}
					>
						{(item._factors.indexOf(fItem.code) >= 0 && _.findWhere(item.effects, { _factor: fItem.code }).effect === 'negative') ? '-' : '+'}
					</button>
				</div>
			));

			return (
				<div key={'variable_' + index} style={[styles.variableList, styles.variableListItem]}>
					<div style={styles.variableListColumnName}>
						<input 
							type="text" 
							placeholder="Variable Name" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'name')} 
							onBlur={this.onBlurInput} 
							value={item.name}
						/>
					</div>
					<div style={styles.variableListColumnDescription}>
						<input 
							type="text" 
							placeholder="Factor Description" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'description')} 
							onBlur={this.onBlurInput} 
							value={item.description}
						/>
					</div>
					<div style={styles.variableListColumnType}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'type')} 
							onBlur={this.onBlurInput} 
							value={item.type}
						>
							<option value="competency">Competency</option>
							<option value="behavioral">Behavioral</option>
						</select>
					</div>
					<div style={styles.variableListColumnFactors}>
						{renderFactors}
					</div>
					<div style={styles.variableListColumnInterpretation}>
						<div>
							<div style={{display: 'inline-block', width: '25%'}}>
								Low:&nbsp;
							</div>
							<input 
								type="text" 
								placeholder="No interpretation" 
								style={{width: '70%'}} 
								onChange={this.onChangeInputInterpretation.bind(this, index, 'low')} 
								onBlur={this.onBlurInput} 
								value={item.interpretation.low}
							/>
						</div>
						<div>
							<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
								Fairy Low:&nbsp;
							</div>
							<input 
								type="text" 
								placeholder="No interpretation" 
								style={{width: '70%'}} 
								onChange={this.onChangeInputInterpretation.bind(this, index, 'fLow')} 
								onBlur={this.onBlurInput} 
								value={item.interpretation.fLow}
							/>
						</div>
						<div>
							<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
								Average:&nbsp;
							</div>
							<input 
								type="text" 
								placeholder="No interpretation" 
								style={{width: '70%'}} 
								onChange={this.onChangeInputInterpretation.bind(this, index, 'average')} 
								onBlur={this.onBlurInput} 
								value={item.interpretation.average}
							/>
						</div>
						<div>
							<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
								Fairy High:&nbsp;
							</div>
							<input 
								type="text" 
								placeholder="No interpretation" 
								style={{width: '70%'}} 
								onChange={this.onChangeInputInterpretation.bind(this, index, 'fHigh')} 
								onBlur={this.onBlurInput} 
								value={item.interpretation.fHigh}
							/>
						</div>
						<div>
							<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
								High:&nbsp;
							</div>
							<input 
								type="text" 
								placeholder="No interpretation" 
								style={{width: '70%'}} 
								onChange={this.onChangeInputInterpretation.bind(this, index, 'high')} 
								onBlur={this.onBlurInput} 
								value={item.interpretation.high}
							/>
						</div>
					</div>
					<div style={styles.variableListColumnAction}>
						<div 
							key={['factor', index, 'delete'].join('_')} 
							style={[styles.btnAction, styles.btnActionDelete]} 
							onClick={this.onClickDeleteVariable.bind(this, index)}
						>Delete</div>
					</div>
				</div>
			);
		});

		return (
			<div style={styles.container}>
				<div style={[styles.variableList, styles.variableListHeader]}>
					<div style={styles.variableListColumnName}>Variable Name</div>
					<div style={styles.variableListColumnDescription}>Description</div>
					<div style={styles.variableListColumnType}>Type</div>
					<div style={styles.variableListColumnFactors}>Factors</div>
					<div style={styles.variableListColumnInterpretation}>Interpretation</div>
					<div style={styles.variableListColumnAction}>Action</div>
				</div>
				{(variables.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No variable has been add so far.</i>
					</div>
				) : null}
				{(variables.length > 0) ? (
					renderVariables
				) : null}
				<div style={styles.newVariableForm}>
					<div style={styles.variableListColumnName}>
						<input 
							type="text" 
							placeholder="Variable Name" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewVariableInput.bind(this, 'name')} 
							value={newVariable.name}
						/>
					</div>
					<div style={styles.variableListColumnDescription}>
						<input 
							type="text" 
							placeholder="Description" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewVariableInput.bind(this, 'description')} 
							value={newVariable.description}
						/>
					</div>
					<div style={styles.variableListColumnType}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeNewVariableInput.bind(this, 'type')} 
							value={newVariable.type}
						>
							<option value="competency">Competency</option>
							<option value="behavioral">Behavioral</option>
						</select>
					</div>
					<div style={styles.variableListColumnFactors}>
						&nbsp;
					</div>
					<div style={styles.variableListColumnInterpretation}>
						&nbsp;
					</div>
					<div style={styles.variableListColumnAction}>
						<div key={'variable_add'} style={[styles.btnAction, styles.btnActionAdd]} onClick={this.onClickAddVariable}>
							Add
						</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(ProductEditorVariables);