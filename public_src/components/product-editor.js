import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import Shuffle from 'knuth-shuffle';

import ServerApiHelper from '../helpers/server-api-helper';

import ProductEditorFactors from './product-editor-factors';
import ProductEditorVariables from './product-editor-variables';
import ProductEditorStatments from './product-editor-statements';
import ProductEditorDimensions from './product-editor-dimensions';
import ProductEditorTreatments from './product-editor-treatments';
import ProductEditorStrengthImprovementNeeds from './product-editor-strength-improvement-needs';
import ProductEditorExaminations from './product-editor-examinations';

import CKEditor from 'react-ckeditor-wrapper';

const styles = {
	container: {
		display: 'flex',
		width: '100vw',
		justifyContent: 'center'
	},

	editorContainer: {
		marginTop: 40,
		marginBottom: 40,
		padding: 20,
		width: 1024,
		border: '1px solid #ccc',
		borderRadius: 10
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	}
};

class ProductEditor extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			product: null,
			factorCodes: [],
			dimensionCodes: [],
			variableCodes: [],
			newBundleCredit: '',
			newBundlePrice: '',
			awardExaminationUsername: '',
			awardExaminationTotal: 0
		};

		this.onChangeInput = this.onChangeInput.bind(this);
		this.onChangeInterpretationLevelInput = this.onChangeInterpretationLevelInput.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onChangeData = this.onChangeData.bind(this);
		this.onChangeCKEditor = this.onChangeCKEditor.bind(this);
	}

	onChangeInput(name, event) {
		var product = this.state.product;

		product[name] = event.target.value.trim();

		this.setState({
			product
		});
	}

	onChangeNewBundleInput(name, event) {
		var state = {};

		state[name] = event.target.value;
		this.setState(state);
	}

	onChangeBundleInput(idx, name, event) {
		var product = this.state.product;

		product.bundles[idx][name] = event.target.value;

		this.setState({
			product
		});
	}

	onChangeAwardExamination(name, event) {
		var state = {};

		state[name] = event.target.value;
		this.setState(state);
	}

	onChangeInterpretationLevelInput(name, event) {
		var product = this.state.product;

		product.interpretationLevel[name] = event.target.value.trim();

		this.setState({
			product
		});
	}

	onChangeCKEditor(html) {
		// var product = this.state.product;

		// product.reportHeader = html;

		// this.setState({
		// 	product
		// }, function () {
		// 	this.onBlurInput();
		// });
	}

	onBlurInput() {
		var _this = this,
			product = this.state.product,
			data = _.omit(product, ['_id', 'createdAt', 'updatedAt']);

		data.reportHeader = data.reportHeader.replace(/\n/g, '');

		if (data.reportTemplate && data.reportTemplate.length === 0) {
			data.reportTemplate = null;
		}

		ServerApiHelper.updateProduct(this.props.match.params.productId, data, function (error, product) {
			_this.setState({
				product
			});
		});
	}

	onClickAwardExamination() {
		var _this = this,
			username = this.state.awardExaminationUsername,
			totalExamination = parseInt(this.state.awardExaminationTotal);

		ServerApiHelper.getAccountByUsername(username, function (error, account) {
			if (!error) {
				var name = account.name,
					email = account.email,
					type = account.type;

				var confirm = prompt([
					'Are you sure to award total ' + totalExamination + ' examination credit for this product to:\n',
					'Name: ' + name,
					'Username: ' + username,
					'Email: ' + email,
					'Sector: ' + type + '\n',
					'Type "yes" to proceed.'
				].join('\n'));

				if (confirm && confirm.toLowerCase() === 'yes') {
					ServerApiHelper.awardProductExamination(account._id, _this.state.product._id, totalExamination, function (error, account) {
						if (!error) {
							alert('Successfully award ' + totalExamination + ' examination credit to ' + username);
							_this.setState({
								awardExaminationUsername: '',
								awardExaminationTotal: ''
							});
						} else {
							alert(error);
						}
					});
				}
			} else {
				alert(error);
			}
		});
	}

	onClickAddBundle() {
		var _this = this,
			product = this.state.product,
			data = _.omit(product, ['_id', 'createdAt', 'updatedAt']);

		data.bundles.push({
			totalExamination: this.state.newBundleCredit,
			totalPrice: this.state.newBundlePrice
		});

		ServerApiHelper.updateProduct(this.props.match.params.productId, data, function (error, product) {
			_this.setState({
				product,
				newBundleCredit: '',
				newBundlePrice: ''
			});
		});
	}

	onClickDeleteBundle(idx) {
		var _this = this,
			product = this.state.product,
			data = _.omit(product, ['_id', 'createdAt', 'updatedAt']);

		data.bundles.splice(idx, 1);

		ServerApiHelper.updateProduct(this.props.match.params.productId, data, function (error, product) {
			_this.setState({
				product
			});
		});
	}

	onChangeData(field, fieldData) {
		console.log('onChangeData', field, fieldData);

		var stateFactorCodes = this.state.factorCodes,
			stateDimensionCodes = this.state.dimensionCodes,
			stateVariableCodes = this.state.variableCodes;

		var _this = this,
			product = _this.state.product;

		product[field] = fieldData;

		var data = _.omit(product, ['_id', 'createdAt', 'updatedAt']);

		var cleanFromFactor = function (data, factorCode) {
			// lets clean dimensions
			for (i = 0; i < data.dimensions.length; i++) {
				var factorIdx = data.dimensions[i]._factors.indexOf(factorCode);
				if (factorIdx !== -1) {
					data.dimensions[i]._factors.splice(factorIdx, 1);
					data.dimensions[i].interpretations.splice(factorIdx, 1);
					data.dimensions[i].traits.splice(factorIdx, 1);
				}
			}

			// lets clean variables
			for (i = 0; i < data.variables.length; i++) {
				var factorIdx = data.variables[i]._factors.indexOf(factorCode);
				if (factorIdx !== -1) {
					data.variables[i]._factors.splice(factorIdx, 1);
				} 
			}

			// lets clean treatments
			for (i = 0; i < data.treatments.length; i++) {
				if (data.treatments[i].linkTo === 'factor') {
					var factorIdx = data.treatments[i]._links.indexOf(factorCode);
					if (factorIdx !== -1) {
						data.treatments[i]._links.splice(factorIdx, 1);
						data.treatments[i].interpretations.splice(factorIdx, 1);
					}
				}
			}

			// lets clean statements
			for (i = 0; i < data.statements.length; i++) {
				if (data.statements[i].characteristic._factor === factorCode) {
					data.statements.splice(i, 1);
					console.log('statements', data.statements);
					i--;
				}
			}

			return data;
		};

		var cleanFromDimension = function (data, dimensionCode) {
			// lets clean treatments
			for (i = 0; i < data.treatments.length; i++) {
				if (data.treatments[i].linkTo === 'dimension') {
					var dimensionIdx = data.treatments[i]._links.indexOf(dimensionCode);
					if (dimensionIdx !== -1) {
						data.treatments[i]._links.splice(dimensionIdx, 1);
						data.treatments[i].interpretations.splice(dimensionIdx, 1);
					}
				}
			}

			return data;
		};

		var cleanFromVariable = function (data, variableName) {
			// lets clean treatments
			for (i = 0; i < data.treatments.length; i++) {
				if (data.treatments[i].linkTo === 'variable') {
					var variableIdx = data.treatments[i]._links.indexOf(variableName);
					if (variableIdx !== -1) {
						data.treatments[i]._links.splice(variableIdx, 1);
						data.treatments[i].interpretations.splice(variableIdx, 1);
					}
				}
			}

			return data;
		};

		switch(field) {
			case 'factors':
				var factorCodes = _.pluck(data.factors, 'code');

				console.log('state', stateFactorCodes);
				console.log('current', factorCodes);

				if (stateFactorCodes.length >= factorCodes.length) {
					var diff = _.difference(stateFactorCodes, factorCodes);
					console.log('diff', diff);
					for (var i = 0; i < diff.length; i++) {
						data = cleanFromFactor(data, diff[i]);
					}
				}
				break;
			case 'dimensions':
				var dimensionCodes = _.pluck(data.dimensions, 'code');

				if (stateDimensionCodes.length >= dimensionCodes.length) {
					var diff = _.difference(stateDimensionCodes, dimensionCodes);
					for (var i = 0; i < diff.length; i++) {
						data = cleanFromDimension(data, diff[i]);
					}
				}
				break;
			case 'variables':
				var variableCodes = _.pluck(data.variables, 'code');

				if (stateVariableCodes.length >= variableCodes.length) {
					var diff = _.difference(stateVariableCodes, variableCodes);
					for (var i = 0; i < diff.length; i++) {
						data = cleanFromVariable(data, diff[i]);
					}
				}
				break;
			case 'statements':
				var statementIds = _.pluck(data.statements, '_id'),
					shuffleStamentIds = Shuffle.knuthShuffle(statementIds);

				data.statementsRandomOrder = shuffleStamentIds;
				break;
		}

		console.log('Update', data);
		ServerApiHelper.updateProduct(this.props.match.params.productId, data, function (error, product) {
			console.log('onChangeData After Update', product);
			var factorCodes = _.pluck(product.factors, 'code'),
				dimensionCodes = _.pluck(product.dimensions, 'code'),
				variableCodes = _.pluck(product.variables, 'code');

			_this.setState({
				product,
				factorCodes,
				dimensionCodes,
				variableCodes
			});
		});
	}

	componentDidMount() {
		var _this = this;

		ServerApiHelper.getProductById(this.props.match.params.productId, function (error, product) {
			var factorCodes = _.pluck(product.factors, 'code'),
				dimensionCodes = _.pluck(product.dimensions, 'code'),
				variableCodes = _.pluck(product.variables, 'code');

			_this.setState({
				product,
				factorCodes,
				dimensionCodes,
				variableCodes
			});
		});
	}

	render() {
		var _this = this;
		var product = this.state.product;

		var productBundles = null;

		if (product) {
			productBundles = product.bundles.map((item, index) => (
				<div key={['bundle', item._id].join('_')} style={{marginBottom: '5px'}}>
					<div style={{display: 'inline-block'}}>
						<input 
							type="number" 
							placeholder="Total credit" 
							onChange={this.onChangeBundleInput.bind(this, index, 'totalExamination')} 
							onBlur={this.onBlurInput.bind(this)} 
							value={item.totalExamination} 
						/>
					</div>
					<div style={{display: 'inline-block'}}>&nbsp;Credits for <b>MYR</b>&nbsp;</div>
					<div style={{display: 'inline-block', marginRight: '5px'}}>
						<input 
							type="number" 
							placeholder="Total price" 
							onChange={this.onChangeBundleInput.bind(this, index, 'totalPrice')} 
							onBlur={this.onBlurInput.bind(this)} 
							value={item.totalPrice} 
						/>
					</div>
					<div style={{display: 'inline-block'}}>
						<div 
							key={['bundle', 'delete', item._id].join('_')}
							style={[styles.btnAction, styles.btnActionDelete]} 
							onClick={this.onClickDeleteBundle.bind(this, index)}
						>Delete</div>
					</div>
				</div>
			));
		}

		return (
			<div style={styles.container}>
				<div style={styles.editorContainer}>
					<div>
						<div style={{display: 'inline-block', width: '50%'}}>
							<div style={{display: 'inline-block'}}>
								<b>Product:&nbsp;</b>
							</div>
							<input 
								type="text" 
								placeholder="Name" 
								onChange={this.onChangeInput.bind(this, 'name')} 
								onBlur={this.onBlurInput} 
								value={(product) ? product.name : ''} 
							/>
						</div>
						<div style={{display: 'inline-block', width: '50%', textAlign: 'right'}}>
							<div style={{display: 'inline-block'}}>
								<b>Sector:&nbsp;</b>
							</div>
							<input type="text" placeholder="Name" value={(product) ? product.sector.toUpperCase() : ''} disabled />
						</div>
					</div>
					<div style={{marginTop: 20}}>
						<div><b>Price Bundles:&nbsp;</b></div>
						<div>
							{productBundles}
							<div>
								<div style={{display: 'inline-block'}}>
									<input 
										type="number" 
										placeholder="Total credit" 
										onChange={this.onChangeNewBundleInput.bind(this, 'newBundleCredit')}  
										value={this.state.newBundleCredit} 
									/>
								</div>
								<div style={{display: 'inline-block'}}>&nbsp;Credits for <b>MYR</b>&nbsp;</div>
								<div style={{display: 'inline-block', marginRight: '5px'}}>
									<input 
										type="number" 
										placeholder="Total price" 
										onChange={this.onChangeNewBundleInput.bind(this, 'newBundlePrice')}  
										value={this.state.newBundlePrice} 
									/>
								</div>
								<div style={{display: 'inline-block'}}>
									<div 
										key="btnActionAddBundle" 
										style={[styles.btnAction, styles.btnActionAdd]} 
										onClick={this.onClickAddBundle.bind(this)}
									>Add New</div>
								</div>
							</div>
						</div>
					</div>
					<div style={{marginTop: 20}}>
						<div><b>Award Examination Credit:&nbsp;</b></div>
						<div>
							<div style={{display: 'inline-block'}}>
								<input 
									type="number" 
									placeholder="Total credit" 
									onChange={this.onChangeAwardExamination.bind(this, 'awardExaminationTotal')}  
									value={this.state.awardExaminationTotal} 
								/>
							</div>
							<div style={{display: 'inline-block'}}>&nbsp;to&nbsp;</div>
							<div style={{display: 'inline-block', marginRight: '5px'}}>
								<input 
									type="text" 
									placeholder="Username" 
									onChange={this.onChangeAwardExamination.bind(this, 'awardExaminationUsername')}  
									value={this.state.awardExaminationUsername} 
								/>
							</div>
							<div style={{display: 'inline-block'}}>
								<div 
									key="btnActionAwardCredit" 
									style={[styles.btnAction, styles.btnActionAdd]} 
									onClick={this.onClickAwardExamination.bind(this)}
								>Award</div>
							</div>
						</div>
					</div>
					<div style={{marginTop: 20}}>
						<div style={{display: 'inline-block'}}>
							<b>Report Template:&nbsp;</b>
						</div>
						<div style={{display: 'inline-block'}}>
							<input 
								type="text" 
								placeholder="Default" 
								onChange={this.onChangeInput.bind(this, 'reportTemplate')} 
								onBlur={this.onBlurInput} 
								value={(product) ? product.reportTemplate : ''} 
							/>
						</div>
					</div>
					<div style={{marginTop: 20}}>
						<div style={{display: 'inline-block'}}>
							<b>Status:&nbsp;</b>
						</div>
						<div style={{display: 'inline-block'}}>
							<select 
								onChange={this.onChangeInput.bind(this, 'status')} 
								onBlur={this.onBlurInput} 
								value={(product) ? product.status : 'draft'} 
							>
								<option value="draft">Draft</option>
								<option value="publish">Publish</option>
							</select>
						</div>
					</div>
					<div style={{marginTop: 20}}>
						<b>Last Modified:</b> {(product) ? product.updatedAt : ''}
					</div>
					<div style={{marginTop: 20}}>
						<div>
							<div style={{display: 'inline-block', width: '17%'}}>&nbsp;</div>
							<div style={{display: 'inline-block', width: '10%'}}>Low</div>
							<div style={{display: 'inline-block', width: '10%'}}>Fairly Low</div>
							<div style={{display: 'inline-block', width: '10%'}}>Average</div>
							<div style={{display: 'inline-block', width: '10%'}}>Fairly High</div>
							<div style={{display: 'inline-block', width: '10%'}}>High</div>
						</div>
						<div>
							<div style={{display: 'inline-block', width: '17%'}}><b>Score Range Setting:</b></div>
							<div style={{display: 'inline-block', width: '10%'}}>
								<input 
									type="number" 
									placeholder="Not set" 
									style={{width: '90%'}} 
									onChange={this.onChangeInterpretationLevelInput.bind(this, 'low')} 
									onBlur={this.onBlurInput} 
									value={(product) ? product.interpretationLevel.low : ''} 
								/>
							</div>
							<div style={{display: 'inline-block', width: '10%'}}>
								<input 
									type="number" 
									placeholder="Not set" 
									style={{width: '90%'}} 
									onChange={this.onChangeInterpretationLevelInput.bind(this, 'fLow')} 
									onBlur={this.onBlurInput} 
									value={(product) ? product.interpretationLevel.fLow : ''} 
								/>
							</div>
							<div style={{display: 'inline-block', width: '10%'}}>
								<input 
									type="number" 
									placeholder="Not set" 
									style={{width: '90%'}} 
									onChange={this.onChangeInterpretationLevelInput.bind(this, 'average')} 
									onBlur={this.onBlurInput} 
									value={(product) ? product.interpretationLevel.average : ''} 
								/>
							</div>
							<div style={{display: 'inline-block', width: '10%'}}>
								<input 
									type="number" 
									placeholder="Not set" 
									style={{width: '90%'}} 
									onChange={this.onChangeInterpretationLevelInput.bind(this, 'fHigh')} 
									onBlur={this.onBlurInput} 
									value={(product) ? product.interpretationLevel.fHigh : ''} 
								/>
							</div>
							<div style={{display: 'inline-block', width: '10%'}}>
								<input 
									type="number" 
									placeholder="Not set" 
									style={{width: '90%'}} 
									onChange={this.onChangeInterpretationLevelInput.bind(this, 'high')} 
									onBlur={this.onBlurInput} 
									value={(product) ? product.interpretationLevel.high : ''} 
								/>
							</div>
						</div>
					</div>
					<div style={{marginTop: 20}}>
						<ul className="nav nav-tabs" role="tablist">
							<li role="presentation" className="active"><a href="#factors" aria-controls="factors" role="tab" data-toggle="tab">Factors</a></li>
							<li role="presentation"><a href="#variables" aria-controls="variables" role="tab" data-toggle="tab">Variables</a></li>
							<li role="presentation"><a href="#dimensions" aria-controls="dimensions" role="tab" data-toggle="tab">Dimensions</a></li>
							<li role="presentation"><a href="#treatments" aria-controls="treatments" role="tab" data-toggle="tab">Treatments</a></li>
							<li role="presentation"><a href="#strengthsimprovements" aria-controls="strengthsimprovements" role="tab" data-toggle="tab">Strengths and Improvements</a></li>
							<li role="presentation"><a href="#statements" aria-controls="statements" role="tab" data-toggle="tab">Statements</a></li>
							<li role="presentation"><a href="#report" aria-controls="report" role="tab" data-toggle="tab">Report Header</a></li>
							<li role="presentation"><a href="#examinations" aria-controls="examinations" role="tab" data-toggle="tab">Examinations</a></li>
						</ul>
						<div className="tab-content">
							<div role="tabpanel" className="tab-pane active" id="factors">
								{(product) ? (
									<ProductEditorFactors factors={product.factors} onChangeData={this.onChangeData} />
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="variables">
								{(product) ? (
									<ProductEditorVariables factors={product.factors} variables={product.variables} onChangeData={this.onChangeData} />
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="dimensions">
								{(product) ? (
									<ProductEditorDimensions factors={product.factors} dimensions={product.dimensions} onChangeData={this.onChangeData} />
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="treatments">
								{(product) ? (
									<ProductEditorTreatments 
										factors={product.factors} 
										dimensions={product.dimensions} 
										variables={product.variables} 
										treatments={product.treatments} 
										onChangeData={this.onChangeData} 
									/>
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="strengthsimprovements">
								{(product) ? (
									<ProductEditorStrengthImprovementNeeds 
										factors={product.factors} 
										onChangeData={this.onChangeData} 
									/>
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="statements">
								{(product) ? (
									<ProductEditorStatments factors={product.factors} statements={product.statements} onChangeData={this.onChangeData} />
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="report">
								{(product) ? (
									<div style={{marginTop: 10}}>
										<CKEditor 
											config={{
												on: {
													blur: function (event) {
														var product = _this.state.product;

														product.reportHeader = event.editor.getData();

														_this.setState({
															product: product
														}, function () {
															_this.onBlurInput();
														});
													}
												}
											}} 
											onChange={this.onChangeCKEditor.bind(this)} 
											value={product.reportHeader} 
										/>
									</div>
								) : null}
							</div>
							<div role="tabpanel" className="tab-pane" id="examinations">
								{(product) ? (
									<ProductEditorExaminations productId={product._id} />
								) : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(ProductEditor);