import React from 'react';
import Radium from 'radium';

const styles = {
	container: {
		marginTop: 10
	},

	dimensionList: {
		width: '100%',
		paddingBottom: 5,
	},

	dimensionListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	dimensionListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	dimensionListColumnCode: {
		display: 'inline-block',
		width: '15%',
		textAlign: 'left'
	},

	dimensionListColumnName: {
		display: 'inline-block',
		width: '25%',
		textAlign: 'left'
	},

	dimensionListColumnDescription: {
		display: 'inline-block',
		width: '50%',
		textAlign: 'left'
	},

	dimensionListColumnAction: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	dimensionListRowFactor: {
		marginTop: 5,
		border: '1px solid #87CEFA',
		backgroundColor: '#ffffff',
		padding: 10
	},

	dimensionListColumnInterpretation: {
		display: 'inline-block',
		width: '50%',
		textAlign: 'left',
		paddingTop: 5
	},

	dimensionListColumnTrait: {
		display: 'inline-block',
		width: '50%',
		textAlign: 'left',
		paddingTop: 5
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},

	newDimensionForm: {
		marginTop: 20,
		paddingTop: 10,
		paddingBottom: 10,
		borderBottom: '1px dashed #ccc'
	},
};

class ProductEditorDimensions extends React.Component {
	constructor(props) {
		super(props);

		// to handle previous variable that not have effect assign
		var dimensions = props.dimensions;

		for (var i = 0; i < dimensions.length; i++) {
			var factors = dimensions[i]._factors,
				effects = dimensions[i].effects;

			if (factors.length > 0 && effects.length === 0) {
				for (var j = 0; j < factors.length; j++) {
					dimensions[i].effects.push({
						_factor: factors[j],
						effect: 'positive',
						value: 1
					});
				}
			}
		}
		// END

		this.state = {
			factors: props.factors,
			dimensions,
			newDimension: {
				code: '',
				name: '',
				description: ''
			}
		}

		this.onChangeInput = this.onChangeInput.bind(this);
		this.onChangeInputFactors = this.onChangeInputFactors.bind(this);
		this.onChangeInputInterpretations = this.onChangeInputInterpretations.bind(this);
		this.onChangeInputTraits = this.onChangeInputTraits.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onChangeNewDimensionInput = this.onChangeNewDimensionInput.bind(this);
		this.onClickAddDimension = this.onClickAddDimension.bind(this);
		this.onClickDeleteDimension = this.onClickDeleteDimension.bind(this);
	}

	onChangeInput(idx, name, event) {
		var dimensions = this.state.dimensions;

		dimensions[idx][name] = event.target.value;

		if (name === 'code') {
			dimensions[idx].code = dimensions[idx].code.toUpperCase();
		}

		this.setState({
			dimensions
		});
	}

	onChangeInputFactors(idx, factorCode, event) {
		var dimensions = this.state.dimensions;

		if (event.target.checked) {
			if (dimensions[idx]._factors.indexOf(factorCode) === -1) {
				dimensions[idx]._factors.push(factorCode);

				dimensions[idx].effects.push({
					_factor: factorCode,
					effect: 'positive',
					value: 1
				});

				dimensions[idx].interpretations.push({
					_factor: factorCode,
					low: '',
					fLow: '',
					average: '',
					fHigh: '',
					high: ''
				});

				dimensions[idx].traits.push({
					_factor: factorCode,
					low: '',
					fLow: '',
					average: '',
					fHigh: '',
					high: ''
				});
			}
		} else {
			var fCodeIdx = dimensions[idx]._factors.indexOf(factorCode);
			if (fCodeIdx >= 0) {
				dimensions[idx]._factors.splice(fCodeIdx, 1);
				dimensions[idx].effects.splice(fCodeIdx, 1);
				dimensions[idx].interpretations.splice(fCodeIdx, 1);
				dimensions[idx].traits.splice(fCodeIdx, 1);
			}
		}

		this.props.onChangeData('dimensions', dimensions);
	}

	onChangeInputEffect(idx, fIdx, event) {
		var dimensions = this.state.dimensions;

		dimensions[idx].effects[fIdx].effect = event.target.value;

		this.props.onChangeData('dimensions', dimensions);
	}

	onChangeInputInterpretations(idx, fIdx, name, event) {
		var dimensions = this.state.dimensions;

		dimensions[idx].interpretations[fIdx][name] = event.target.value;

		this.setState({
			dimensions
		});
	}

	onChangeInputTraits(idx, fIdx, name, event) {
		var dimensions = this.state.dimensions;

		dimensions[idx].traits[fIdx][name] = event.target.value;

		this.setState({
			dimensions
		});
	}

	onBlurInput() {
		var dimensions = this.state.dimensions;
		this.props.onChangeData('dimensions', dimensions);
	}

	onChangeNewDimensionInput(name, event) {
		var newDimension = this.state.newDimension;

		newDimension[name] = event.target.value;

		if (name === 'code') {
			newDimension.code = newDimension.code.toUpperCase();
		}

		this.setState({
			newDimension
		});
	}

	onClickAddDimension() {
		var dimensions = this.state.dimensions,
			newDimension = this.state.newDimension;

		dimensions.push({
			code: newDimension.code.trim(),
			name: newDimension.name.trim(),
			description: newDimension.description.trim(),
			_factors: [],
			effects: [],
			interpretations: [],
			traits: []
		});

		newDimension = {
			code: '',
			name: '',
			description: ''
		};

		this.setState({
			newDimension
		});

		this.props.onChangeData('dimensions', dimensions);
	}

	onClickDeleteDimension(idx) {
		var confirm = prompt('Are you sure to delete this dimension?\nType "yes" to continue.');

		if (confirm && confirm.toLowerCase() === 'yes') {
			var dimensions = this.state.dimensions;

			dimensions.splice(idx, 1);
			
			this.props.onChangeData('dimensions', dimensions);
		}
	}

	componentWillReceiveProps(props) {
		this.setState({
			factors: props.factors,
			dimensions: props.dimensions
		});
	}

	render() {
		var factors = this.state.factors,
			dimensions = this.state.dimensions,
			newDimension = this.state.newDimension;

		var renderDimensions = dimensions.map((item, index) => {
			var renderFactors = factors.map((fItem, fIndex) => (
				<label 
					key={['dimension', index, 'factor', fIndex].join('_')} 
					style={{marginRight: 10}}
				>
					<input 
						type="checkbox" 
						onChange={this.onChangeInputFactors.bind(this, index, fItem.code)} 
						checked={(item._factors.indexOf(fItem.code) >= 0) ? true : false} 
					/>&nbsp;{fItem.code}
				</label>
			));

			var renderFactorsTab = item._factors.map((fItem, fIndex) => (
				<li 
					key={['dimension', index, 'factor', fIndex, 'tab'].join('_')} 
					role="presentation" className={(fIndex === 0) ? 'active' : null}
				>
					<a 
						href={['#dimension', index ,'factor', fItem].join('_')} 
						aria-controls={['dimension', index ,'factor', fItem].join('_')} 
						role="tab" 
						data-toggle="tab"
					>{fItem}</a>
				</li>
			));

			var renderFactorsTabContent = item._factors.map((fItem, fIndex) => (
				<div 
					key={['dimension', index, 'factor', fIndex, 'tab', 'content'].join('_')}
					role="tabpanel" 
					className={(fIndex === 0) ? 'tab-pane active' : 'tab-pane'} 
					id={['dimension', index ,'factor', fItem].join('_')}
				>
					<div style={[styles.dimensionList], {marginTop: '10px'}}>
						<span style={{fontWeight: 'bold'}}>Effect:&nbsp;</span>
						<select 
							onChange={this.onChangeInputEffect.bind(this, index, fIndex)} 
							value={item.effects[fIndex].effect}
						>
							<option value="positive">Positive</option>
							<option value="negative">Negative</option>
						</select>
					</div>
					<div style={[styles.dimensionList, styles.dimensionListHeader]}>
						<div style={styles.dimensionListColumnInterpretation}>Interpretations</div>
						<div style={styles.dimensionListColumnTrait}>Traits</div>
					</div>
					<div style={[styles.dimensionList]}>
						<div style={styles.dimensionListColumnInterpretation}>
							<div>
								<div style={{display: 'inline-block', width: '25%'}}>
									Low:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No interpretation" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputInterpretations.bind(this, index, fIndex, 'low')} 
									onBlur={this.onBlurInput} 
									value={item.interpretations[fIndex].low}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									Fairy Low:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No interpretation" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputInterpretations.bind(this, index, fIndex, 'fLow')} 
									onBlur={this.onBlurInput} 
									value={item.interpretations[fIndex].fLow}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									Average:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No interpretation" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputInterpretations.bind(this, index, fIndex, 'average')} 
									onBlur={this.onBlurInput} 
									value={item.interpretations[fIndex].average}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									Fairy High:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No interpretation" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputInterpretations.bind(this, index, fIndex, 'fHigh')} 
									onBlur={this.onBlurInput} 
									value={item.interpretations[fIndex].fHigh}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									High:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No interpretation" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputInterpretations.bind(this, index, fIndex, 'high')} 
									onBlur={this.onBlurInput} 
									value={item.interpretations[fIndex].high}
								/>
							</div>
						</div>
						<div style={styles.dimensionListColumnTrait}>
							<div>
								<div style={{display: 'inline-block', width: '25%'}}>
									Low:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No trait" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputTraits.bind(this, index, fIndex, 'low')} 
									onBlur={this.onBlurInput} 
									value={item.traits[fIndex].low}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									Fairy Low:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No trait" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputTraits.bind(this, index, fIndex, 'fLow')} 
									onBlur={this.onBlurInput} 
									value={item.traits[fIndex].fLow}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									Average:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No trait" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputTraits.bind(this, index, fIndex, 'average')} 
									onBlur={this.onBlurInput} 
									value={item.traits[fIndex].average}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									Fairy High:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No trait" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputTraits.bind(this, index, fIndex, 'fHigh')} 
									onBlur={this.onBlurInput} 
									value={item.traits[fIndex].fHigh}
								/>
							</div>
							<div>
								<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
									High:&nbsp;
								</div>
								<input 
									type="text" 
									placeholder="No trait" 
									style={{width: '70%'}} 
									onChange={this.onChangeInputTraits.bind(this, index, fIndex, 'high')} 
									onBlur={this.onBlurInput} 
									value={item.traits[fIndex].high}
								/>
							</div>
						</div>
					</div>
				</div>
			));

			return (
				<div key={['dimension', index].join('_')} style={[styles.dimensionList, styles.dimensionListItem]}>
					<div style={styles.dimensionListColumnCode}>
						<input 
							type="text" 
							placeholder="Dimension code" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'code')} 
							onBlur={this.onBlurInput} 
							value={item.code}
						/>
					</div>
					<div style={styles.dimensionListColumnName}>
						<input 
							type="text" 
							placeholder="Name" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'name')} 
							onBlur={this.onBlurInput} 
							value={item.name}
						/>
					</div>
					<div style={styles.dimensionListColumnDescription}>
						<input 
							type="text" 
							placeholder="Description" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'description')} 
							onBlur={this.onBlurInput} 
							value={item.description}
						/>
					</div>
					<div style={styles.dimensionListColumnAction}>
						<div 
							key={['dimension', index, 'delete'].join('_')} 
							style={[styles.btnAction, styles.btnActionDelete]} 
							onClick={this.onClickDeleteDimension.bind(this, index)}
						>Delete</div>
					</div>
					<div style={styles.dimensionListRowFactor}>
						<div>
							{renderFactors}
						</div>
						<div style={{marginTop: 20}}>
							<ul className="nav nav-tabs" role="tablist">
								{renderFactorsTab}
							</ul>
							<div className="tab-content">
								{renderFactorsTabContent}
							</div>
						</div>
					</div>
				</div>
			);
		});

		return (
			<div style={styles.container}>
				<div style={[styles.dimensionList, styles.dimensionListHeader]}>
					<div style={styles.dimensionListColumnCode}>Dimension Code</div>
					<div style={styles.dimensionListColumnName}>Name</div>
					<div style={styles.dimensionListColumnDescription}>Description</div>
					<div style={styles.dimensionListColumnAction}>Action</div>
				</div>
				{(dimensions.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No dimension has been add so far.</i>
					</div>
				) : null}
				{(dimensions.length > 0) ? (
					renderDimensions
				) : null}
				<div style={styles.newDimensionForm}>
					<div style={styles.dimensionListColumnCode}>
						<input 
							type="text" 
							placeholder="Dimension Code" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewDimensionInput.bind(this, 'code')} 
							value={newDimension.code}
						/>
					</div>
					<div style={styles.dimensionListColumnName}>
						<input 
							type="text" 
							placeholder="Name" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewDimensionInput.bind(this, 'name')} 
							value={newDimension.name}
						/>
					</div>
					<div style={styles.dimensionListColumnDescription}>
						<input 
							type="text" 
							placeholder="Description" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewDimensionInput.bind(this, 'description')} 
							value={newDimension.description}
						/>
					</div>
					<div style={styles.dimensionListColumnAction}>
						<div 
							key={['dimension', 'add'].join('_')} 
							style={[styles.btnAction, styles.btnActionAdd]} 
							onClick={this.onClickAddDimension}
						>Add</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(ProductEditorDimensions);