import React from 'react';
import Radium from 'radium';

const styles = {
	container: {
		marginTop: 10
	},

	factorList: {
		width: '100%',
		paddingBottom: 5,
	},

	factorListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	factorListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	factorListColumnCode: {
		display: 'inline-block',
		width: '20%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	factorListColumnName: {
		display: 'inline-block',
		width: '20%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	factorListColumnInterpretation: {
		display: 'inline-block',
		width: '60%',
		verticalAlign: 'top',
		textAlign: 'left'
	}
};

class ProductEditorStrengthImprovementNeeds extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			factors: props.factors
		};

		this.onChangeInputInterpretation = this.onChangeInputInterpretation.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
	}

	onChangeInputInterpretation(idx, levelName, event) {
		var factors = this.state.factors;

		factors[idx].interpretation[levelName] = event.target.value;

		this.setState({
			factors
		});
	}

	onBlurInput() {
		var factors = this.state.factors;
		this.props.onChangeData('factors', factors);
	}

	componentWillReceiveProps(props) {
		this.setState({
			factors: props.factors
		});
	}

	render() {
		var factors = this.state.factors;

		var renderFactors = factors.map((item, index) => (
			<div key={'factor_sin_' + index} style={[styles.factorList, styles.factorListItem]}>
				<div style={styles.factorListColumnCode}>
					<input 
						type="text" 
						placeholder="Factor Code" 
						style={{width: '90%'}} 
						value={item.code} 
						disabled="disabled"
					/>
				</div>
				<div style={styles.factorListColumnName}>
					<input 
						type="text" 
						placeholder="Name" 
						style={{width: '90%'}} 
						value={item.name} 
						disabled="disabled"
					/>
				</div>
				<div style={styles.factorListColumnInterpretation}>
					<div>
						<div style={{display: 'inline-block', width: '25%'}}>
							Low:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No interpretation" 
							style={{width: '70%'}} 
							onChange={this.onChangeInputInterpretation.bind(this, index, 'low')} 
							onBlur={this.onBlurInput} 
							value={item.interpretation.low}
						/>
					</div>
					<div>
						<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
							Fairy Low:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No interpretation" 
							style={{width: '70%'}} 
							onChange={this.onChangeInputInterpretation.bind(this, index, 'fLow')} 
							onBlur={this.onBlurInput} 
							value={item.interpretation.fLow}
						/>
					</div>
					<div>
						<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
							Average:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No interpretation" 
							style={{width: '70%'}} 
							onChange={this.onChangeInputInterpretation.bind(this, index, 'average')} 
							onBlur={this.onBlurInput} 
							value={item.interpretation.average}
						/>
					</div>
					<div>
						<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
							Fairy High:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No interpretation" 
							style={{width: '70%'}} 
							onChange={this.onChangeInputInterpretation.bind(this, index, 'fHigh')} 
							onBlur={this.onBlurInput} 
							value={item.interpretation.fHigh}
						/>
					</div>
					<div>
						<div style={{display: 'inline-block', width: '25%', marginTop: 5}}>
							High:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No interpretation" 
							style={{width: '70%'}} 
							onChange={this.onChangeInputInterpretation.bind(this, index, 'high')} 
							onBlur={this.onBlurInput} 
							value={item.interpretation.high}
						/>
					</div>
				</div>
			</div>
		));

		return (
			<div style={styles.container}>
				<div style={[styles.factorList, styles.factorListHeader]}>
					<div style={styles.factorListColumnCode}>Factor Code</div>
					<div style={styles.factorListColumnName}>Name</div>
					<div style={styles.factorListColumnInterpretation}>Strength & Improvement Need</div>
				</div>
				{(factors.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No factor has been add so far.</i>
					</div>
				) : null}
				{(factors.length > 0) ? (
					renderFactors
				) : null}
			</div>
		);
	}
}

module.exports = Radium(ProductEditorStrengthImprovementNeeds);