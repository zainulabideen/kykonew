import React from 'react';
import Radium from 'radium';

import async from 'async';

import ServerApiHelper from '../helpers/server-api-helper';

const styles = {
	container: {
		display: 'flex',
		width: '100vw',
		justifyContent: 'center'
	},

	listContainer: {
		width: 1024,
		marginTop: 40,
		marginBottom: 40,
		padding: 20,
		border: '1px solid #000000',
		borderRadius: 10
	},

	productList: {
		width: '100%',
		paddingBottom: 5,
	},

	productListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	productListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	productListColumnName: {
		display: 'inline-block',
		width: '40%',
		textAlign: 'left'
	},

	productListColumnVariables: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	productListColumnFactors: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	productListColumnStatements: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	productListColumnActions: {
		display: 'inline-block',
		width: '30%',
		textAlign: 'center'
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionEdit: {
		borderColor: 'green',
		color: 'green'
	},

	btnActionDuplicate: {
		borderColor: 'blue',
		color: 'blue'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},

	newProductForm: {
		marginTop: 20,
		paddingTop: 10,
		paddingBottom: 10,
		borderTop: '1px dashed #ccc',
		borderBottom: '1px dashed #ccc'
	},

	processingContainer: {
		position: 'absolute',
		top: 0,
		left: 0,
		width: '100vw',
		height: '100vh',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'rgba(0, 0, 0, 0.4)',
		color: '#ffffff'
	},
};

class ProductList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isProcessing: true,
			products: [],
			newProductName: ''
		};

		this.onChangeInput = this.onChangeInput.bind(this);
		this.onClickAddProduct = this.onClickAddProduct.bind(this);
		this.onClickEditProduct = this.onClickEditProduct.bind(this);
		this.onClickDuplicateProduct = this.onClickDuplicateProduct.bind(this);
		this.onClickDeleteProduct = this.onClickDeleteProduct.bind(this);
	}

	onChangeInput(event) {
		this.setState({
			newProductName: event.target.value
		});
	}

	onClickAddProduct(event) {
		if (this.state.newProductName.trim().length > 0) {
			var data = {
				sector: this.props.match.params.sectorName,
				name: this.state.newProductName.trim()
			};

			this.setState({
				isProcessing: true
			});

			var _this = this;

			ServerApiHelper.addProduct(data, function(error, product) {
				if (error) {
					console.error(error);
					_this.setState({
						isProcessing: false
					});
				} else {
					var existingProducts = _this.state.products;
					existingProducts.push(product);

					console.log(existingProducts);

					_this.setState({
						isProcessing: false,
						products: existingProducts,
						newProductName: ''
					});
				}
			});
		}
	}

	onClickEditProduct(id) {
		this.props.history.push('/sectors/' + this.props.match.params.sectorName + '/products/' + id + '/editor');
	}

	onClickDuplicateProduct(id) {
		this.setState({
			isProcessing: true
		});

		var _this = this;

		ServerApiHelper.duplicateProduct(id, function(error, product) {
			if (error) {
				console.error(error);
				_this.setState({
					isProcessing: false
				});
			} else {
				var existingProducts = _this.state.products;
				existingProducts.push(product);

				console.log(existingProducts);

				_this.setState({
					isProcessing: false,
					products: existingProducts
				});
			}
		});
	}

	onClickDeleteProduct(id) {
		var confirm = prompt('Are you sure want to delete this product?\nType "yes" to continue.');

		if (confirm && confirm.toLowerCase() === 'yes') {
			this.setState({
				isProcessing: true
			});

			var _this = this;

			async.waterfall([
				function (callback) {
					ServerApiHelper.deleteProduct(id, function(error, product) {
						callback(error);
					});
				},

				function (callback) {
					ServerApiHelper.getProducts({sector: _this.props.match.params.sectorName}, callback);
				}
			], function (error, products) {
				if (error) {
					console.error(error);
					_this.setState({
						isProcessing: false
					});
				} else {
					_this.setState({
						isProcessing: false,
						products
					});
				}
			});
		}
	}

	componentDidMount() {
		this.setState({
			isProcessing: true
		});

		var _this = this;

		ServerApiHelper.getProducts({sector: this.props.match.params.sectorName}, function (error, products) {
			if (error) {
				console.error(error);
				_this.setState({
					isProcessing: false
				});
			} else {
				_this.setState({
					isProcessing: false,
					products
				});
			}
		});
	}

	render() {
		const renderProducts = this.state.products.map((item, index) => (
			<div key={'product_' + index} style={[styles.productList, styles.productListItem]}>
				<div style={styles.productListColumnName}>{item.name}</div>
				<div style={styles.productListColumnVariables}>{item.variables.length}</div>
				<div style={styles.productListColumnFactors}>{item.factors.length}</div>
				<div style={styles.productListColumnStatements}>{item.statements.length}</div>
				<div style={styles.productListColumnActions}>
					<div 
						key={['product', index, 'edit'].join('_')} 
						style={[styles.btnAction, styles.btnActionEdit]} 
						onClick={this.onClickEditProduct.bind(this, item._id)}
					>Edit</div>
					<div 
						key={['product', index, 'duplicate'].join('_')} 
						style={[styles.btnAction, styles.btnActionDuplicate]} 
						onClick={this.onClickDuplicateProduct.bind(this, item._id)}
					>Duplicate</div>
					<div 
						key={['product', index, 'delete'].join('_')} 
						style={[styles.btnAction, styles.btnActionDelete]} 
						onClick={this.onClickDeleteProduct.bind(this, item._id)}
					>Delete</div>
				</div>
			</div>
		));

		return (
			<div style={styles.container}>
				<div style={styles.listContainer}>
					{(this.state.products.length === 0) ? (
						<div>
							<i>No product has been add so far.</i>
						</div>
					) : (
						<div style={[styles.productList, styles.productListHeader]}>
							<div style={styles.productListColumnName}>Product Name</div>
							<div style={styles.productListColumnVariables}>Variables</div>
							<div style={styles.productListColumnFactors}>Factors</div>
							<div style={styles.productListColumnStatements}>Statements</div>
							<div style={styles.productListColumnActions}>Actions</div>
						</div>
					)}
					{(this.state.products.length > 0) ? (
						renderProducts
					) : null}
					<div style={styles.newProductForm}>
						<div style={{display: 'inline-block', width: '40%', textAlign: 'left'}}>
							<input 
								type="text" 
								placeholder="Product Name" 
								style={{width: '100%'}} 
								onChange={this.onChangeInput} 
								value={this.state.newProductName}
							/>
						</div>
						<div style={{display: 'inline-block', width: '60%', textAlign: 'right'}}>
							<div key={'product_add'} style={[styles.btnAction, styles.btnActionAdd]} onClick={this.onClickAddProduct}>
								Add Product
							</div>
						</div>
					</div>
				</div>
				{(this.state.isProcessing) ? (
					<div style={styles.processingContainer}>
						<h1>Processing. Please wait...</h1>
					</div>
				) : null}
			</div>
		);
	}
}

module.exports = Radium(ProductList);