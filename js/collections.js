module.exports = function (meijin) {
	meijin.collections.add('products', {
		status: { type: String, default: 'draft', enum: ['draft', 'publish'] },
		sector: { type: String, default: null, enum: ['personal', 'corporate', 'education'] },
		name: { type: String, default: null },
		bundles: [{
			totalExamination: { type: Number, default: null },
			totalPrice: { type: Number, default: null },
			additionalInformation: { type: String, default: null }
		}],
		reportHeader: { type: String, default: null },
		factors: [
			{
				code: { type: String, default: null },
				name: { type: String, default: null },
				description: { type: String, default: null },
				// in report render as "strength and improvement needs" (split in different form)
				interpretation: {
					// improvement
					low: { type: String, default: null },
					fLow: { type: String, default: null },
					average: { type: String, default: null },
					// strength
					fHigh: { type: String, default: null },
					high: { type: String, default: null }
				}
			}
		],
		interpretationLevel: {
			low: { type: Number, default: 0 },
			fLow: { type: Number, default: 20 },
			average: { type: Number, default: 40 },
			fHigh: { type: Number, default: 60 },
			high: { type: Number, default: 80 }
		},
		dimensions: [{
			code: { type: String, default: null },
			name: { type: String, default: null },
			description: { type: String, default: null },
			// dynamic add option contain form below
			_factors: [String],
			// each factor must have effect
			effects: [{
				_factor: { type: String, default: null },
				effect: { type: String, enum: ['positive', 'negative'], default: 'positive' },
				value: { type: Number, default: 1 }
			}],
			// each factor must have interpretation
			interpretations: [{
				_factor: { type: String, default: null },
				low: { type: String, default: null },
				fLow: { type: String, default: null },
				average: { type: String, default: null },
				fHigh: { type: String, default: null },
				high: { type: String, default: null }
			}],
			// each factor must have traits
			traits: [{
				_factor: { type: String, default: null },
				low: { type: String, default: null },
				fLow: { type: String, default: null },
				average: { type: String, default: null },
				fHigh: { type: String, default: null },
				high: { type: String, default: null }
			}]
		}],
		variables: [
			{
				name: { type: String, default: null },
				description: { type: String, default: null },
				// select
				type: { type: String, enum: ['competency', 'behavioral'], default: null },
				_factors: [String],
				// each factor must have effect
				effects: [{
					_factor: { type: String, default: null },
					effect: { type: String, enum: ['positive', 'negative'], default: 'positive' },
					value: { type: Number, default: 1 }
				}],
				interpretation: {
					low: { type: String, default: null },
					fLow: { type: String, default: null },
					average: { type: String, default: null },
					fHigh: { type: String, default: null },
					high: { type: String, default: null }
				}
			}
		],
		treatments: [
			{
				name: { type: String, default: null },
				linkTo: { type: String, enum: ['factor', 'dimension', 'variable'] },
				_links: [String],
				// dynamic add option contain form below
				interpretations: [{
					_link: { type: String, default: null },
					low: { type: String, default: null },//placeholder "no value present" equal to null
					fLow: { type: String, default: null },
					average: { type: String, default: null },
					fHigh: { type: String, default: null },
					high: { type: String, default: null }
				}]
			}
		],
		statements: [
			{
				text: { type: String, default: null },
				characteristic: {
					_factor: { type: String, default: null },
					effect: { type: String, enum: ['positive', 'negative'], default: 'positive' },
					value: { type: Number, default: 1 }
				}
			}
		],
		statementsRandomOrder: [String], // undefined value in array tell us that the last statement created
		reportTemplate: { type: String, default: null }
	});

	meijin.collections.add('examinations', {
		_product: { type: String, default: null },
		_account: { type: String, default: null },
		email: { type: String, default: null },
		additionalInformation: { type: String, default: null },
		invitedAt: { type: Date, default: Date.now },
		answers: [{
			_statement: { type: String, default: null },
			value: { type: Number, default: null }
		}],
		result: {
			factors: [
				{
					code: { type: String, default: null },
					score: { type: Number, default: null }
				}
			],
			dimensions: [
				{
					code: { type: String, default: null },
					score: { type: Number, default: null }
				}
			],
			variables: [
				{
					code: { type: String, default: null },
					score: { type: Number, default: null }
				}
			]
		},
		isCompleted: { type: Boolean, default: false }
	});

	meijin.collections.add('accounts', {
		username: { type: String, default: null },
		password: { type: String, default: null },
		name: { type: String, default: null },
		address: { type: String, default: null },
		email: { type: String, default: null },
		type: { type: String, default: 'personal', enum: ['personal', 'corporate', 'education'] },
		availableExaminations: [{
			_product: { type: String, default: null },
			total: { type: Number, default: null }
		}],
		purchaseHistories: [{
			_product: { type: String, default: null },
			totalExamination: { type: Number, default: null },
			totalAmount: { type: Number, default: null },
			purchasedAt: { type: Date, default: Date.now },
			transactionInfo: { type: Object, default: null }
		}]
	});

	meijin.collections.add('password_resets', {
		_account: { type: String, default: null }
	});
};