var async = require('async'),
	request = require('request'),
	_ = require('underscore'),
	bcrypt = require('bcryptjs');
	var sys = require('sys');
var JSON = require('JSON');
var path = require('path');
const base64 = require('base64topdf');
var bodyParser = require('body-parser');
var fs = require('fs');
var pdf = require('dynamic-html-pdf');

function _hashPassword(password) {
	return bcrypt.hashSync(password);
}

function _comparePasswordHash(password, hash) {
	return bcrypt.compareSync(password, hash);
}

module.exports = function (meijin) {
	// PAGES
	/////////
	meijin.router.get('/admin', function (req, res) {
		res.render('admin');
	});

	meijin.router.get('/', function (req, res) {
		res.render('main');
	});

	meijin.router.get('/app', function (req, res) {
		res.render('main');
	});

	// ACCOUNT API
	///////////////
	meijin.router.post('/api/:apiVersion/register', function (req, res) {
		var apiVersion = req.params.apiVersion || null;

		var username = req.body.username || null,
			name = req.body.name || null,
			email = req.body.email || null,
			address = req.body.address || null,
			type = req.body.type || null;

		async.waterfall([
			function (callback) {
				if (username && name && email && address && type) {
					callback();
				} else {
					callback('Insuficient parameter.');
				}
			},

			function (callback) {
				var _params = {
					where: {
						username: username
					}
				};

				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/accounts?_params=' + JSON.stringify(_params),
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						if (result.data.length === 0) {
							callback();
						} else {
							callback('Username has been taken.');
						}
					} else {
						if (result && result.error) {
							callback(result.error);
						} else {
							callback(error);
						}
					}
				});
			},

			function (callback) {
				request({
					method: 'POST',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/accounts',
					json: {
						username: username,
						name: name,
						address: address,
						email: email,
						type: type
					}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback(null, result.data);
					} else {
						if (result && result.error) {
							callback(result.error);
						} else {
							callback(error);
						}
					}
				});
			},

			function (account, callback) {
				request({
					method: 'POST',
					url: 'http://127.0.0.1:' + req.config.port + '/api/v1/passwordreset',
					json: {
						username: account.username
					}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback(null, account);
					} else {
						if (result && result.error) {
							callback(result.error);
						} else {
							callback(error);
						}
					}
				});
			},

			function (account, callback) {
				callback(null, _.omit(account, 'password'));
			}
		], function (error, data) {

			if (!error) {


//     });

				res.status(200).json({
					status: 200,
					success: true,
					data: data
				});
			} else {
				res.status(400).json({
					status: 400,
					success: false,
					error: error
				});
			}
		});
	});

	meijin.router.post('/api/:apiVersion/login', function (req, res) {
		var apiVersion = req.params.apiVersion || null;

		var username = req.body.username || null,
			password = req.body.password || null;

		async.waterfall([
			// find account base on username and password
			function (callback) {
				var _params = {
					where: {
						username: username
					}
				};

				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/accounts?_params=' + JSON.stringify(_params),
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						if (result.data.length > 0) {
							callback(null, result.data[0]);
						} else {
							callback('Invalid username or password.');
						}
					} else {
						if (result && result.error) {
							callback(result.error);
						} else {
							callback(error);
						}
					}
				});
			},

			function (account, callback) {
				if (_comparePasswordHash(account.username + password, account.password)) {
					callback(null, account);
				} else {
					callback('Invalid username or password.');
				}
			},

			// remove sensitive data
			function (account, callback) {
				callback(null, _.omit(account, 'password'));
			}
		], function (error, data) {
			if (!error) {
				res.status(200).json({
					status: 200,
					success: true,
					data: data
				});
			} else {
				res.status(400).json({
					status: 400,
					success: false,
					error: error
				});
			}
		});
	});

	meijin.router.post('/api/:apiVersion/passwordreset', function (req, res) {
		var apiVersion = req.params.apiVersion || null,
			username = req.body.username || null;

		async.waterfall([
			function (callback) {
				var _params = {
					where: {
						username: username
					}
				};

				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/accounts?_params=' + JSON.stringify(_params),
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						if (result.data.length > 0) {
							callback(null, result.data[0]);
						} else {
							callback('Invalid username.');
						}
					} else {
						if (result && result.error) {
							callback(result.error);
						} else {
							callback(error);
						}
					}
				});
			},

			function (account, callback) {
				// Prepare reset password link
				request({
					method: 'POST',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/password_resets',
					json: {
						_account: account._id
					}
				}, function (error, response, result) {
					if (!error && result.success) {
						var resetLink = 'http://' + req.config.host + '/passwordreset?token=' + result.data._id;
						callback(null, account, resetLink);
					} else {
						callback((error) ? error : result.error);
					}
				});
			},

			function (account, resetLink, callback) {
				request({
					method: 'POST',
					url: 'http://127.0.0.1:' + req.config.port + '/core/emails',
					json: {
						to: account.email,
						content: {
							subject: 'Kyko Password Reset.',
							html: [
								'Someone has requested password reset for account, <b>' + account.username + '</b>.',
								'<br/><br/>',
								'Click on following link to proceed, <a href="' + resetLink + '">RESET PASSWORD</a>'
							].join(''),
							text: [
								'Someone has requested password reset for account, ' + account.username + '.',
								'\n\n',
								'Click on following link to proceed, ' + resetLink,
							].join('')
						}
					}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback()
					} else {
						callback((error) ? error : result.error);
					}
				});
			}
		], function (error) {
			if (!error) {
				res.status(200).json({
					status: 200,
					success: true
				});
			} else {
				res.status(400).json({
					status: 400,
					success: false,
					error: error
				});
			}
		});
	});

	meijin.router.get('/passwordreset', function (req, res) {
		var token = req.query.token || null;

		async.waterfall([
			function (callback) {
				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/password_resets/' + token,
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback(null, result.data._account);
					} else {
						callback((error) ? error : result.error);
					}
				});
			}
		], function (error, accountId) {
			if (!error) {
				res.render('passwordreset', { error: error, token: token, accountId: accountId });
			} else {
				res.send('Unable to process your request. Please make sure the link was correct and it never been used before.');
			}
		});
	});

	meijin.router.post('/passwordchange', function (req, res) {
		var token = req.body.inputHiddenToken || null,
			accountId = req.body.inputHiddenAccountId || null,
			email = req.body.inputTxtEmail || null,
			password = req.body.inputTxtPassword || null,
			password2 = req.body.inputTxtPassword2 || null;

		async.waterfall([
			function (callback) {
				if (password !== null && password.length > 0 && password === password2) {
					callback();
				} else {
					callback('Confirm password not match with password. Please enter the same value.');
				}
			},

			function (callback) {
				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/password_resets/' + token,
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						if (result.data._account === accountId) {
							callback();
						} else {
							callback('Unable to process your request. Please make sure the link was correct and it never been used before.');
						}
					} else {
						callback((error) ? error : result.error);
					}
				});
			},

			function (callback) {
				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/accounts/' + accountId,
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						if (result.data.email === email) {
							callback(null, result.data.username);
						} else {
							callback('Email not match with given token. Please make sure the link was correct and it never been used before.');
						}
					} else {
						callback((error) ? error : result.error);
					}
				});
			},

			function (username, callback) {
				var hashPassword = _hashPassword(username + password);

				request({
					method: 'PUT',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/accounts/' + accountId,
					json: {
						password: hashPassword
					}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback();
					} else {
						callback((error) ? error : result.error);
					}
				});
			},

			function (callback) {
				request({
					method: 'DELETE',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/password_resets/' + token,
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback();
					} else {
						callback((error) ? error : result.error);
					}
				});
			}
		], function (error) {
			if (!error) {
				res.send('Password successfuly change.');
			} else {
				res.render('forgotpassword', { error: error, token: token, accountId: accountId });
			}
		});
	});


// send mail with defined transport object

 meijin.router.post('/sendreport', function (req, res) {
		// var pdflink = 'http://104.248.139.234:3000/getreport?name='+req.body.reportname.replace(/['"]+/g, '').replace(/\s/g, "_")+'.pdf';
			var pdflink = 'http://139.59.66.215:3000/getreport?name='+req.body.reportname.replace(/['"]+/g, '').replace(/\s/g, "_")+'.pdf';

			console.log(req.body.reportname);
		async.waterfall([
			function (callback) {
			request({
				method: 'POST',
				url: 'http://127.0.0.1:' + req.config.port + '/core/emails',
				json: {
					to: req.body.email.replace(/['"]+/g, ''),
					content: {
						subject: 'Kyko Report.',
						html: [
							'Congratulations Your report is here, <b>',
							'<br/><br/>',
							'Click on following link to proceed, <a href="' + pdflink + '">Download Report</a>'
						].join(''),
						text: [
							'Congratulations Your report is here, ' + '.',
							'\n\n',
							'Click on following link to proceed, ' + pdflink,
						].join('')
					}
				}
			}, function (error, response, result) {
				if (!error && result.success) {
					callback()
					console.log("inside success");
				} else {
					console.log(error);
					callback((error) ? error : result.error);
				}
			});
		}
	], function (error) {
		if (!error) {
			res.status(200).json({
				status: 200,
				success: true
			});
		} else {
			res.status(400).json({
				status: 400,
				success: false,
				error: error
			});
		}
	});

// });
});

meijin.router.post('/uploadPdf', function (req, res) {
 var apiVersion = req.params.apiVersion || null,
	 email = req.body.email || null;

	 //create pdf

	 var data = req.body;
	 // console.log(req.body.reportname);
	 console.log("SaquibPDF");
	 // console.log(data);
					 fs.writeFile('reports/'+req.body.reportname.replace(/['"]+/g, '').replace(/\s/g, "_") +'.pdf', data.data, {encoding: 'base64'}, function(err) {
						 if (!err) {
	 				 	res.status(200).json({
	 				 		status: 200,
	 				 		success: true
	 				 	});
	 				 	} else {
	 				 	res.status(400).json({
	 				 		status: 400,
	 				 		success: false,
	 				 		error: err
	 				 	});
	 				 	}
					});
});

	// EXAMINATION REPORT
	//////////////////////
	meijin.router.get('/report', function (req, res) {
		var examinationId = req.query.eid || 'null';

		async.waterfall([
			function (callback) {
				request({
					method: 'GET',
					url: 'http://127.0.0.1:' + req.config.port + '/core/collections/examinations/' + examinationId,
					json: {}
				}, function (error, response, result) {
					if (!error && result.success) {
						callback(null, result.data);
					} else {
						callback((error) ? error : result.error);
					}
				});
			},

			function (examination, callback) {
				if (examination.isCompleted) {
					request({
						method: 'GET',
						url: 'http://127.0.0.1:' + req.config.port + '/core/collections/products/' + examination._product,
						json: {}
					}, function (error, response, result) {
						if (!error && result.success) {
							callback(null, examination, result.data);

						} else {
							callback((error) ? error : result.error);
						}
					});
				} else {
					callback('Complete examination require to generate report.');
				}
			},

			function (examination, product, callback) {
				var _scoreLevelHelper = function (score, retType) {
					retType = retType || 'default';

					var percent = score / 7 * 100;

					if (percent >= product.interpretationLevel.low && percent < product.interpretationLevel.fLow) {
						switch (retType) {
							case 'level':
								return 'low';
							case 'percent':
								return percent;
							default:
								return 'Low';
						}
					} else if (percent >= product.interpretationLevel.fLow && percent < product.interpretationLevel.average) {
						switch (retType) {
							case 'level':
								return 'fLow';
							case 'percent':
								return percent;
							default:
								return 'Fairly Low';
						}
					} else if (percent >= product.interpretationLevel.average && percent < product.interpretationLevel.fHigh) {
						switch (retType) {
							case 'level':
								return 'average';
							case 'percent':
								return percent;
							default:
								return 'Average';
						}
					} else if (percent >= product.interpretationLevel.fHigh && percent < product.interpretationLevel.high) {
						switch (retType) {
							case 'level':
								return 'fHigh';
							case 'percent':
								return percent;
							default:
								return 'Fairly High';
						}
					} else if (percent >= product.interpretationLevel.high) {
						switch (retType) {
							case 'level':
								return 'high';
							case 'percent':
								return percent;
							default:
								return 'High';
						}
					}
				}

				var report = {
					name: product.name,
					candidate: {
						email: examination.email,
						additionalInformation: examination.additionalInformation
					},
					header: product.reportHeader,
					dimensions: [],
					variables: [],
					factor: {
						strengths: [],
						improvements: []
					},
					recomendations: []
				};

				var i = 0;

				// dimension
				for (i = 0; i < examination.result.dimensions.length; i++) {
					var result = examination.result.dimensions[i],
						dimension = _.findWhere(product.dimensions, { code: result.code });

					var dimensionReport = {
						name: dimension.name,
						score: result.score.toFixed(2),
						level: _scoreLevelHelper(result.score),
						interpretation: '',
						traits: [],
						factors: []
					};

					for (var j = 0; j < product.factors.length; j++) {
						var factorCode = product.factors[j].code,
							factorName = product.factors[j].name,
							factorDesc = product.factors[j].description;
var codefirst = factorCode.charAt(0)
var codesecond= factorCode.substring(0, 2);
var codeforsummary= factorCode.substring(0, 2)+j;
						if (dimension._factors.indexOf(factorCode) === -1) {
							continue;
						}

						var factorScore = _.findWhere(examination.result.factors, { code: factorCode }).score,
							factorScoreLevel = _scoreLevelHelper(factorScore, 'level');

						var factorInterpretation = _.findWhere(dimension.interpretations, { _factor: factorCode });
						dimensionReport.interpretation += (((dimensionReport.interpretation.length > 0) ? ' ' : '') + factorInterpretation[factorScoreLevel]);

						var factorTrait = _.findWhere(dimension.traits, { _factor: factorCode });
						dimensionReport.traits.push(factorTrait[factorScoreLevel]);

						dimensionReport.factors.push({
							codefirst:codefirst,
							codesecond:codesecond,
							codesummary:codeforsummary,
							code: factorCode,
							name: factorName,
							description: factorDesc,
							score: factorScore.toFixed(2),
							percent: _scoreLevelHelper(factorScore, 'percent')
						});
					}

					report.dimensions.push(dimensionReport);
					console.log(dimensionReport);
				}

				// variable
				for (i = 0; i < examination.result.variables.length; i++) {
					var result = examination.result.variables[i],
						variable = _.findWhere(product.variables, { name: result.code });

					var variableReport = {
						type: variable.type,
						name: variable.name,
						score: result.score.toFixed(2),
						level: _scoreLevelHelper(result.score),
						interpretation: '',
						factors: []
					};

					var variableScoreLevel = _scoreLevelHelper(result.score, 'level');
					variableReport.interpretation = variable.interpretation[variableScoreLevel];

					for (var j = 0; j < product.factors.length; j++) {
						var factorCode = product.factors[j].code,
							factorName = product.factors[j].name,
							factorDesc = product.factors[j].description;
							var codefirst = factorCode.charAt(0)
							var codesecond= factorCode.substring(0, 2);
							var codeforsummary= factorCode.substring(0, 2)+j;
						if (variable._factors.indexOf(factorCode) === -1) {
							continue;
						}
//code summary
						var factorScore = _.findWhere(examination.result.factors, { code: factorCode }).score;

						variableReport.factors.push({
							codefirst:codefirst,
							codesecond:codesecond,
							codesummary:codeforsummary,
							code: factorCode,
							name: factorName,
							description: factorDesc,
							score: factorScore.toFixed(2),
							percent: _scoreLevelHelper(factorScore, 'percent')
						});
					}

					report.variables.push(variableReport);
				}

				// strength & improvement
				for (i = 0; i < examination.result.factors.length; i++) {
					var result = examination.result.factors[i];
						factor = _.findWhere(product.factors, { code: result.code });

					var factorScoreLevel = _scoreLevelHelper(result.score, 'level');

					switch (factorScoreLevel) {
						case 'low':
						case 'fLow':
						case 'average':
							report.factor.improvements.push(factor.interpretation[factorScoreLevel]);
							break;
						case 'fHigh':
						case 'high':
							report.factor.strengths.push(factor.interpretation[factorScoreLevel]);
							break;
					}
				}

				// recomendation & treatment
				for (i = 0; i < product.treatments.length; i++) {
					var treatment = product.treatments[i];
					var linkScores = null;

					switch (treatment.linkTo) {
						case 'factor':
							linkScores = _.filter(examination.result.factors, function (item) {
								return (treatment._links.indexOf(item.code) !== -1)
							});
							break;
						case 'dimension':
							linkScores = _.filter(examination.result.dimensions, function (item) {
								return (treatment._links.indexOf(item.code) !== -1)
							});
							break;
						case 'variable':
							linkScores = _.filter(examination.result.variables, function (item) {
								return (treatment._links.indexOf(item.code) !== -1)
							});
							break;
					}

					if (linkScores === null || linkScores.length === 0) {
						continue;
					}

					var recomendation = {
						name: treatment.name,
						items: []
					};

					for (var j = 0; j < linkScores.length; j++) {
						var text = _.findWhere(
							treatment.interpretations,
							{ _link: linkScores[j].code }
						)[_scoreLevelHelper(linkScores[j].score, 'level')];

						if (text.length > 0) {
							recomendation.items.push(text);
						}
					}

					report.recomendations.push(recomendation);
				}

				callback(null, product, report);
			}
		], function (error, product, data) {
			if (!error) {
				if (product.reportTemplate && product.reportTemplate.length > 0) {
					res.render(product.reportTemplate, { error: error, data: data });

				} else {
					res.render('report', { error: error, data: data });
           // console.log(data);
				}
			} else {
				res.send(error);
			}
		});
	});

	var url = require('url');
	meijin.router.get('/getreport', function (req, res) {


   var file = path.join("./reports", req.query.name);
   res.download(file, function (err) {
       if (err) {
           console.log("Error");
           console.log(err);
       } else {
           console.log("Success");
       }
   });
});


meijin.router.get("/getImage", (req, res) => {
	console.log(req.query.image);
	// console.log(__dirname+"/"+req.query.image);
	// res.sendFile(path.join(__dirname, 'images', 'userPhoto.jpg'))
	var file = path.join("/var/www/html/kykonew/reportimages", req.query.image);
	// var file = path.join("/Users/admin/Downloads/kyko/reportimages", req.query.image);
	console.log(file);
  res.sendFile(file);
});



};
