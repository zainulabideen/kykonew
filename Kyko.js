var meijin = require('./meijin');

require('./js/collections')(meijin);// Collections declaration
require('./js/routes')(meijin);// Custom routes declaration
require('./js/hooks')(meijin);// Hook declaration
require('./js/config')(meijin);// Config declaration

// check for --env on run cli
var envDefined = 1;

if (envDefined !== -1) {
	env = process.argv[envDefined + 1] || 'development';
	meijin.environment(env);
} else {
	meijin.environment('development');
}
// END

meijin.listen();
