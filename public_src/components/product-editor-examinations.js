import React from 'react';
import Radium from 'radium';
import _ from 'underscore';

import ServerApiHelper from '../helpers/server-api-helper';

const styles = {
	container: {
		marginTop: 10
	},

	examinationList: {
		width: '100%',
		paddingBottom: 5,
	},

	examinationListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	examinationListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	examinationListColumnId: {
		display: 'inline-block',
		width: '70%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	examinationListColumnCompleted: {
		display: 'inline-block',
		width: '15%',
		verticalAlign: 'top',
		textAlign: 'left'
	},

	examinationListColumnAction: {
		display: 'inline-block',
		width: '15%',
		verticalAlign: 'top',
		textAlign: 'center'
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionEdit: {
		borderColor: 'blue',
		color: 'blue'
	},

	btnActionReport: {
		borderColor: 'green',
		color: 'green'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},
};

class ProductEditorExaminations extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			productId: props.productId,
			examinations: []
		};
	}

	onClickNewExamination() {
		var _this = this;

		var data = {
			_product: _this.state.productId
		};

		ServerApiHelper.addExamination(data, (error, examination) => {
			if (!error) {
				var examinations = _this.state.examinations;

				examinations.push(examination);

				_this.setState({
					examinations
				}, () => {
					alert('Successfully added new examination.');
				});
			} else {
				alert(error);
			}
		});
	}

	onClickTakeExamination(id) {
		window.open('/app#/products/' + this.state.productId + '/examinations/' + id, '_blank');
	}

	onClickExaminationReport(id) {
		window.open('/report?eid=' + id, '_blank');
	}

	onClickDeleteExamination(id) {
		var _this = this;

		ServerApiHelper.deleteExamination(id, (error, examination) => {
			if (!error) {
				var examinations = _this.state.examinations;
				var idx = _.findIndex(examinations, { _id: id });

				if (idx !== -1) {
					examinations.splice(idx, 1);

					_this.setState({
						examinations
					}, () => {
						alert('Successfully deleted examination.');
					});
				}
			} else {
				alert(error);
			}
		});
	}

	componentDidMount() {
		var _this = this;

		ServerApiHelper.getExaminations({ _product: _this.state.productId, _account: null }, (error, examinations) => {
			if (!error) {
				_this.setState({
					examinations
				});
			} else {
				alert(error);
			}
		});
	}

	render() {
		var renderExaminations = this.state.examinations.map((item, index) => {
			return (
				<div key={'examination_' + index} style={[styles.examinationList, styles.examinationListItem]}>
					<div style={styles.examinationListColumnId}>
						{item._id}
					</div>
					<div style={styles.examinationListColumnCompleted}>
						{(item.isCompleted) ? (
							<div 
								key={['examination', index, 'report'].join('_')} 
								style={[styles.btnAction, styles.btnActionReport]} 
								onClick={this.onClickExaminationReport.bind(this, item._id)}
							>View Report</div>
						) : (<i>Not Complete</i>)}
					</div>
					<div style={styles.examinationListColumnAction}>
						<div 
							key={['examination', index, 'edit'].join('_')} 
							style={[styles.btnAction, styles.btnActionEdit]} 
							onClick={this.onClickTakeExamination.bind(this, item._id)}
						>Take</div>
						<div 
							key={['examination', index, 'delete'].join('_')} 
							style={[styles.btnAction, styles.btnActionDelete]} 
							onClick={this.onClickDeleteExamination.bind(this, item._id)}
						>Delete</div>
					</div>
				</div>
			);
		});

		return (
			<div style={styles.container}>
				<div 
					style={[styles.btnAction, styles.btnActionAdd]} 
					onClick={this.onClickNewExamination.bind(this)}
				>Add Test Examination</div>
				<br/><br/>
				<div style={[styles.examinationList, styles.examinationListHeader]}>
					<div style={styles.examinationListColumnId}>Examination ID</div>
					<div style={styles.examinationListColumnCompleted}>Completed</div>
					<div style={styles.examinationListColumnAction}>Actions</div>
				</div>
				{(this.state.examinations.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No examination has been created so far.</i>
					</div>
				) : null}
				{(this.state.examinations.length > 0) ? (
					renderExaminations
				) : null}
			</div>
		);
	}
}

module.exports = Radium(ProductEditorExaminations);
