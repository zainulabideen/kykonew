import React from 'react';
import Radium from 'radium';

const styles = {
	container: {
		marginTop: 10
	},

	treatmentList: {
		width: '100%',
		paddingBottom: 5,
	},

	treatmentListHeader: {
		fontWeight: 'bold',
		borderBottom: '2px solid #000000'
	},

	treatmentListItem: {
		paddingTop: 10,
		borderBottom: '1px solid #ccc',
		':hover': {
			backgroundColor: '#dcdcdc'
		}
	},

	treatmentListColumnName: {
		display: 'inline-block',
		width: '60%',
		textAlign: 'left'
	},

	treatmentListColumnLinkTo: {
		display: 'inline-block',
		width: '30%',
		textAlign: 'left'
	},

	treatmentListColumnAction: {
		display: 'inline-block',
		width: '10%',
		textAlign: 'center'
	},

	treatmentListRowLink: {
		marginTop: 5,
		border: '1px solid #87CEFA',
		backgroundColor: '#ffffff',
		padding: 10
	},

	btnAction: {
		marginRight: 5,
		display: 'inline-block',
		padding: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		cursor: 'pointer',
		':hover': {
			backgroundColor: '#ccc',
			color: '#ffffff !important'
		}
	},

	btnActionAdd: {
		borderColor: '#000000',
		color: '#000000'
	},

	btnActionDelete: {
		borderColor: 'red',
		color: 'red'
	},

	newTreatmentForm: {
		marginTop: 20,
		paddingTop: 10,
		paddingBottom: 10,
		borderBottom: '1px dashed #ccc'
	},
};

class ProductEditorTreatments extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			factors: props.factors,
			dimensions: props.dimensions,
			variables: props.variables,
			treatments: props.treatments,
			newTreatment: {
				name: '',
				linkTo: 'factor'
			}
		}

		this.onChangeInputLinks = this.onChangeInputLinks.bind(this);
		this.onChangeInputTreatments = this.onChangeInputTreatments.bind(this);
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onChangeInput = this.onChangeInput.bind(this);
		this.onClickDeleteTreatment = this.onClickDeleteTreatment.bind(this);
		this.onChangeNewTreatmentInput = this.onChangeNewTreatmentInput.bind(this);
		this.onClickAddTreatment = this.onClickAddTreatment.bind(this);
	}

	onChangeInputLinks(idx, value, event) {
		var treatments = this.state.treatments;

		if (event.target.checked) {
			if (treatments[idx]._links.indexOf(value) === -1) {
				treatments[idx]._links.push(value);

				treatments[idx].interpretations.push({
					_link: value,
					low: '',
					fLow: '',
					average: '',
					fHigh: '',
					high: ''
				});
			}
		} else {
			var linkIdx = treatments[idx]._links.indexOf(value);
			if (linkIdx >= 0) {
				treatments[idx]._links.splice(linkIdx, 1);
				treatments[idx].interpretations.splice(linkIdx, 1);
			}
		}

		this.props.onChangeData('treatments', treatments);
	}

	onChangeInputTreatments(idx, linkIdx, name, event) {
		var treatments = this.state.treatments;

		treatments[idx].interpretations[linkIdx][name] = event.target.value;

		this.setState({
			treatments
		});
	}

	onBlurInput() {
		var treatments = this.state.treatments;
		this.props.onChangeData('treatments', treatments);
	}

	onChangeInput(idx, name, event) {
		var treatments = this.state.treatments;

		treatments[idx][name] = event.target.value;

		this.setState({
			treatments
		});
	}

	onClickDeleteTreatment(idx) {
		var confirm = prompt('Are you sure to delete this treatment?\nType "yes" to continue.');

		if (confirm && confirm.toLowerCase() === 'yes') {
			var treatments = this.state.treatments;

			treatments.splice(idx, 1);
			
			this.props.onChangeData('treatments', treatments);
		}
	}

	onChangeNewTreatmentInput(name, event) {
		var newTreatment = this.state.newTreatment;

		newTreatment[name] = event.target.value;

		this.setState({
			newTreatment
		});
	}

	onClickAddTreatment() {
		var treatments = this.state.treatments,
			newTreatment = this.state.newTreatment;

		treatments.push({
			name: newTreatment.name.trim(),
			linkTo: newTreatment.linkTo.trim(),
			_links: [],
			interpretations: []
		});

		newTreatment = {
			name: '',
			linkTo: 'factor'
		};

		this.setState({
			newTreatment
		});

		this.props.onChangeData('treatments', treatments);
	}

	componentWillReceiveProps(props) {
		this.setState({
			factors: props.factors,
			dimensions: props.dimensions,
			variables: props.variables,
			treatments: props.treatments
		});
	}

	render() {
		var factors = this.state.factors,
			dimensions = this.state.dimensions,
			variables = this.state.variables,
			treatments = this.state.treatments,
			newTreatment = this.state.newTreatment;

		var renderTreatments = treatments.map((item, index) => {
			var renderLinks = null;

			switch (item.linkTo) {
				case 'factor':
					renderLinks = factors.map((fItem, fIndex) => (
						<label 
							key={['treatment', index, 'factor', fIndex].join('_')} 
							style={{marginRight: 10}}
						>
							<input 
								type="checkbox" 
								onChange={this.onChangeInputLinks.bind(this, index, fItem.code)} 
								checked={(item._links.indexOf(fItem.code) >= 0) ? true : false} 
							/>&nbsp;{fItem.code}
						</label>
					));
					break;
				case 'dimension':
					renderLinks = dimensions.map((dItem, dIndex) => (
						<label 
							key={['treatment', index, 'dimension', dIndex].join('_')} 
							style={{marginRight: 10}}
						>
							<input 
								type="checkbox" 
								onChange={this.onChangeInputLinks.bind(this, index, dItem.code)} 
								checked={(item._links.indexOf(dItem.code) >= 0) ? true : false} 
							/>&nbsp;{dItem.code}
						</label>
					));
					break;
				case 'variable':
					renderLinks = variables.map((vItem, vIndex) => (
						<label 
							key={['treatment', index, 'variable', vIndex].join('_')} 
							style={{marginRight: 10}}
						>
							<input 
								type="checkbox" 
								onChange={this.onChangeInputLinks.bind(this, index, vItem.name)} 
								checked={(item._links.indexOf(vItem.name) >= 0) ? true : false} 
							/>&nbsp;{vItem.name}
						</label>
					));
					break;
			}

			var renderLinksTab = item._links.map((lItem, lIndex) => (
				<li 
					key={['treatment', index, 'link', lIndex, 'tab'].join('_')} 
					role="presentation" className={(lIndex === 0) ? 'active' : null}
				>
					<a 
						href={['#treatment', index ,'link', lItem.replace(/ /g, '')].join('_')} 
						aria-controls={['treatment', index ,'link', lItem.replace(/ /g, '')].join('_')} 
						role="tab" 
						data-toggle="tab"
					>{lItem}</a>
				</li>
			));

			var renderLinksTabContent = item._links.map((lItem, lIndex) => (
				<div 
					key={['treatment', index, 'link', lIndex, 'tab', 'content'].join('_')}
					role="tabpanel" 
					className={(lIndex === 0) ? 'tab-pane active' : 'tab-pane'} 
					id={['treatment', index ,'link', lItem.replace(/ /g, '')].join('_')}
				>
					<div style={{marginTop: 5}}>
						<div style={{display: 'inline-block', width: '10%'}}>
							Low:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No treatment" 
							style={{width: '50%'}} 
							onChange={this.onChangeInputTreatments.bind(this, index, lIndex, 'low')} 
							onBlur={this.onBlurInput} 
							value={item.interpretations[lIndex].low}
						/>
					</div>
					<div style={{marginTop: 5}}>
						<div style={{display: 'inline-block', width: '10%'}}>
							Fairy Low:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No treatment" 
							style={{width: '50%'}} 
							onChange={this.onChangeInputTreatments.bind(this, index, lIndex, 'fLow')} 
							onBlur={this.onBlurInput} 
							value={item.interpretations[lIndex].fLow}
						/>
					</div>
					<div style={{marginTop: 5}}>
						<div style={{display: 'inline-block', width: '10%'}}>
							Average:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No treatment" 
							style={{width: '50%'}} 
							onChange={this.onChangeInputTreatments.bind(this, index, lIndex, 'average')} 
							onBlur={this.onBlurInput} 
							value={item.interpretations[lIndex].average}
						/>
					</div>
					<div style={{marginTop: 5}}>
						<div style={{display: 'inline-block', width: '10%'}}>
							Fairy High:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No treatment" 
							style={{width: '50%'}} 
							onChange={this.onChangeInputTreatments.bind(this, index, lIndex, 'fHigh')} 
							onBlur={this.onBlurInput} 
							value={item.interpretations[lIndex].fHigh}
						/>
					</div>
					<div style={{marginTop: 5}}>
						<div style={{display: 'inline-block', width: '10%'}}>
							High:&nbsp;
						</div>
						<input 
							type="text" 
							placeholder="No treatment" 
							style={{width: '50%'}} 
							onChange={this.onChangeInputTreatments.bind(this, index, lIndex, 'high')} 
							onBlur={this.onBlurInput} 
							value={item.interpretations[lIndex].high}
						/>
					</div>
				</div>
			));

			return (
				<div key={['treatment', index].join('_')} style={[styles.treatmentList, styles.treatmentListItem]}>
					<div style={styles.treatmentListColumnName}>
						<input 
							type="text" 
							placeholder="Treatment name" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'name')} 
							onBlur={this.onBlurInput} 
							value={item.name}
						/>
					</div>
					<div style={styles.treatmentListColumnLinkTo}>
						<input 
							type="text" 
							placeholder="For" 
							style={{width: '90%'}} 
							onChange={this.onChangeInput.bind(this, index, 'linkTo')} 
							onBlur={this.onBlurInput} 
							value={item.linkTo.toUpperCase()} 
							disabled="disabled"
						/>
					</div>
					<div style={styles.treatmentListColumnAction}>
						<div 
							key={['treatment', index, 'delete'].join('_')} 
							style={[styles.btnAction, styles.btnActionDelete]} 
							onClick={this.onClickDeleteTreatment.bind(this, index)}
						>Delete</div>
					</div>
					<div style={styles.treatmentListRowLink}>
						<div>
							{renderLinks}
						</div>
						<div style={{marginTop: 20}}>
							<ul className="nav nav-tabs" role="tablist">
								{renderLinksTab}
							</ul>
							<div className="tab-content">
								{renderLinksTabContent}
							</div>
						</div>
					</div>
				</div>
			);
		});

		return (
			<div style={styles.container}>
				<div style={[styles.treatmentList, styles.treatmentListHeader]}>
					<div style={styles.treatmentListColumnName}>Treatment Name</div>
					<div style={styles.treatmentListColumnLinkTo}>For</div>
					<div style={styles.treatmentListColumnAction}>Action</div>
				</div>
				{(treatments.length === 0) ? (
					<div style={{paddingTop: 5, paddingBottom: 5, borderBottom: '1px solid #ccc'}}>
						<i>No treatment has been add so far.</i>
					</div>
				) : null}
				{(treatments.length > 0) ? (
					renderTreatments
				) : null}
				<div style={styles.newTreatmentForm}>
					<div style={styles.treatmentListColumnName}>
						<input 
							type="text" 
							placeholder="Treatment Name" 
							style={{width: '90%'}} 
							onChange={this.onChangeNewTreatmentInput.bind(this, 'name')} 
							value={newTreatment.name}
						/>
					</div>
					<div style={styles.treatmentListColumnLinkTo}>
						<select 
							style={{width: '90%'}} 
							onChange={this.onChangeNewTreatmentInput.bind(this, 'linkTo')} 
							value={newTreatment.linkTo}
						>
							<option value="factor">Factor</option>
							<option value="dimension">Dimension</option>
							<option value="variable">Variable</option>
						</select>
					</div>
					<div style={styles.treatmentListColumnAction}>
						<div 
							key={['treatment', 'add'].join('_')} 
							style={[styles.btnAction, styles.btnActionAdd]} 
							onClick={this.onClickAddTreatment}
						>Add</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(ProductEditorTreatments);