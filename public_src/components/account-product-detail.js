import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import async from 'async';
import moment from 'moment';

import PaypalExpressBtn from 'react-paypal-express-checkout';
import CKEditor from 'react-ckeditor-wrapper';

import ServerApiHelper from '../helpers/server-api-helper';

import AccountNavigation from './account-navigation';

const styles = {
	container: {
		marginTop: 10
	},
};

class AccountProductDetail extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: true,
			isProcessingPurchase: false,
			product: null,
			examinationCredit: null,
			purchaseHistories: [],
			examinations: [],
			paypalBundleId: null,
			paypalTotal: null,
			candidateEmail: '',
			candidateInfo: ''
		}

		this._syncWithServerData = this._syncWithServerData.bind(this);
	}

	onClickPurchaseBundle(bundleId, bundlePrice, event) {
		this.setState({
			paypalBundleId: bundleId,
			paypalTotal: bundlePrice
		});
	}

	onChangeInput(name, event) {
		var state = {};

		state[name] = event.target.value;

		this.setState(state);
	}

	onChangeCKEditor(html) {
		this.setState({
			candidateInfo: html
		});
	}

	onClickInviteCandidate(event) {
		$('#btnInvite').button('loading');

		if (this.state.candidateEmail.length > 0) {
			var confirm = prompt('Are you sure want to invite ' + this.state.candidateEmail + '?\nExamination credit will be deducted for this invitation.\nType "yes" to continue.');

			if (confirm && confirm.toLowerCase() === 'yes') {
				var _this = this;

				ServerApiHelper.examinationInvitation(
					sessionStorage[this.props.match.params.accountToken],
					this.props.match.params.productId,
					this.state.candidateEmail,
					this.state.candidateInfo.replace(/\n/g, ''),
					function (error, examination) {
						if (!error) {
							alert('Successfully invite ' + examination.email + '.');
							_this.setState({
								candidateEmail: '',
								candidateInfo: ''
							}, function () {
								_this._syncWithServerData();
								$('#btnInvite').button('reset');
							});
						} else {
							alert(error);
						}
					}
				);
			}
		} else {
			alert('Please provide candidate email for the invitation.');
		}
	}

	onClickViewReport(examinationId, event) {
		window.open('/report?eid=' + examinationId, '_blank');
	}

	paypalOnSuccess(payment, event) {
		if (payment.paid) {
			var _this = this;
			var bundle = _.findWhere(this.state.product.bundles, { _id: this.state.paypalBundleId });

			_this.setState({
				isProcessingPurchase: true
			}, function () {
				ServerApiHelper.purchaseProductExamination(
					sessionStorage[_this.props.match.params.accountToken],
					_this.props.match.params.productId,
					bundle.totalExamination,
					_this.state.paypalTotal,
					payment,
					function (error, account) {
						if (!error) {
							alert('Successfully purchased examination credit.');
							_this.setState({
								isProcessingPurchase: false
							}, function () {
								_this._syncWithServerData();
							});
						} else {
							alert(error);
						}
					}
				);
			});
		} else {
			alert('Unable to process your payment. Please contact us to resolve this.');
			console.log('PAYPAL SUCCESS unable to process', payment);
		}
	}

	paypalOnCancel(payment) {
		console.log('PAYPAL PAYMENT CANCEL', payment);
	}

	paypalOnError(error) {
		alert(error);
	}

	_syncWithServerData() {
		var _this = this;

		async.waterfall([
			function (callback) {
				ServerApiHelper.getAccountById(sessionStorage[_this.props.match.params.accountToken], callback);
			},

			function (account, callback) {
				ServerApiHelper.getProductById(_this.props.match.params.productId, function (error, product) {
					callback(error, account, product);
				});
			},

			function (account, product, callback) {
				ServerApiHelper.getExaminations({ _product: product._id, _account: account._id }, function (error, examinations) {
					callback(error, account, product, examinations);
				});
			}
		], function (error, account, product, examinations) {
			if (!error) {
				var availableExamination = _.findWhere(account.availableExaminations, { _product: product._id });

				_this.setState({
					isLoading: false,
					product,
					examinationCredit: (availableExamination) ? availableExamination.total : 0,
					purchaseHistories: _.where(account.purchaseHistories, { _product: product._id }),
					examinations
				});
			} else {
				alert(error);
			}
		});
	}

	componentDidMount() {
		this._syncWithServerData();
	}

	render() {
		var renderBundles = null;

		if (this.state.product) {
			renderBundles = this.state.product.bundles.map((item, index) => (
				<div key={['bundle', item._id].join('_')}>
					<input
						type="radio"
						name="bundle"
						value={[item._id, item.totalExamination, item.totalPrice].join('|')}
						style={{marginRight: 5, verticalAlign: 'top'}}
						onClick={this.onClickPurchaseBundle.bind(this, item._id, item.totalPrice)}
					/>
					<div style={{display: 'inline-block'}}>
						<div>{item.totalExamination} Credits for MYR{item.totalPrice.toFixed(2)}</div>
						{(item.additionalInformation && item.additionalInformation.length > 0) ? (
							<div>
								{item.additionalInformation}
							</div>
						) : null}
					</div>
				</div>
			));
		}

		var renderExaminations = this.state.examinations.map((item, index) => (
			<tr key={['examination', item._id].join('_')}>
				<td style={{textAlign: 'right', width: '1%', whiteSpace: 'nowrap'}}>{index + 1}</td>
				<td>{item.email}</td>
				<td dangerouslySetInnerHTML={{ __html: item.additionalInformation }}></td>
				<td>{moment(item.createdAt).local().format('YYYY-MM-DD HH:mm')}</td>
				<td>{(item.isCompleted) ? (<b>Completed</b>) : (<i>Not Complete</i>)}</td>
				<td style={{textAlign: 'center'}}>
					<button
						className="btn btn-link"
						disabled={(!item.isCompleted) ? 'disabled' : null}
						onClick={this.onClickViewReport.bind(this, item._id)}
					>View Report</button>
				</td>
			</tr>
		));

		var renderPurchases = this.state.purchaseHistories.map((item, index) => (
			<tr key={['purchase', item._id].join('_')}>
				<td style={{textAlign: 'right', width: '1%', whiteSpace: 'nowrap'}}>{index + 1}</td>
				<td>{moment(item.purchasedAt).local().format('YYYY-MM-DD HH:mm')}</td>
				<td>{item.totalExamination}</td>
				<td>MYR{item.totalAmount.toFixed(2)}</td>
			</tr>
		));

		return (
			<div className="container" style={styles.container}>
				<div className="row">
					<AccountNavigation accountToken={this.props.match.params.accountToken} backUrl={'#/accounts/' + this.props.match.params.accountToken + '/products'} />
				</div>
				<div className="row">
					<div className="col-md-12">
						{(!this.state.isLoading) ? (
							<table>
								<tbody>
									<tr>
										<td>Product</td>
										<td>
											<div style={{display: 'inline-block', marginLeft: 10, marginRight: 5}}>:</div>
											<div style={{display: 'inline-block'}}>{this.state.product.name}</div>
										</td>
									</tr>
									<tr>
										<td>Total Examination</td>
										<td>
											<div style={{display: 'inline-block', marginLeft: 10, marginRight: 5}}>:</div>
											<div style={{display: 'inline-block'}}>{this.state.examinations.length}</div>
										</td>
									</tr>
									<tr>
										<td style={{width: '1%', whiteSpace: 'nowrap'}}>Total Examination Credit</td>
										<td>
											<div style={{display: 'inline-block', marginLeft: 10, marginRight: 5}}>:</div>
											<div style={{display: 'inline-block'}}>{this.state.examinationCredit}</div>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<div style={{display: 'inline-block', verticalAlign: 'top', marginLeft: 10, marginRight: 5}}>:</div>
											<div style={{display: 'inline-block'}}>
												<div><b><u>Purchase more credits</u></b></div>
												<div>{renderBundles}</div>
												{(this.state.paypalBundleId) ? (
													<div style={{marginTop: 5}}>
														{(!this.state.isProcessingPurchase) ? (
															<PaypalExpressBtn
																env={'production'} 
																client={{
																	sandbox: 'AflMSwTq7XGYYeNSjLImrN_CIsfhtRSCmLsKlTrxc6lPGXWwPJp8Ay0wbXJjBWoytUrIwGuVcbMJZNVG',
																	production: 'Af1WvyVcxmP--VLg3URiCyEjb-4OWkIR6Y5pKh-dW73E4co8n0FFotisOAY_6HIWFGlHVmi2AB0PZXXQ'
																}}
																currency={'MYR'}
																total={this.state.paypalTotal}
																onError={this.paypalOnError.bind(this)}
																onSuccess={this.paypalOnSuccess.bind(this)}
																onCancel={this.paypalOnCancel.bind(this)}
															/>
														) : (
															<i>Processing. Please wait...</i>
														)}
													</div>
												) : null}
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						) : (
							<h3>Loading product data. Please wait...</h3>
						)}
					</div>
				</div>
				{(!this.state.isLoading) ? (
					<div className="row" style={{marginTop: 40}}>
						<div className="col-md-12">
							<ul className="nav nav-tabs" role="tablist">
								<li role="presentation" className="active"><a href="#examinations" aria-controls="examinations" role="tab" data-toggle="tab">Examinations</a></li>
								<li role="presentation"><a href="#purchases" aria-controls="purchases" role="tab" data-toggle="tab">Purchases</a></li>
							</ul>
							<div className="tab-content">
								<div role="tabpanel" className="tab-pane active" id="examinations">
									<div className="panel panel-default" style={{marginTop: 10}}>
										<div className="panel-heading">Invite Candidate</div>
										<div className="panel-body">
											<div className="form-group">
												<label>Email Address</label>
												<input
													type="email"
													className="form-control"
													placeholder="Candidate email"
													onChange={this.onChangeInput.bind(this, 'candidateEmail')}
													value={this.state.candidateEmail}
												/>
											</div>
											<div className="form-group">
												<label>Additional Information</label>
												<CKEditor onChange={this.onChangeCKEditor.bind(this)} value={this.state.candidateInfo} />
											</div>
											<button
												id="btnInvite"
												className="btn btn-default"
												disabled={(this.state.examinationCredit === 0) ? 'disabled' : null}
												onClick={this.onClickInviteCandidate.bind(this)}
											>Invite Candidate</button>
										</div>
									</div>
									<table className="table" style={{marginTop: 20}}>
										<thead>
											<tr>
												<th>#</th>
												<th>Email</th>
												<th>Additional Information</th>
												<th>Invitation Date</th>
												<th>Status</th>
												<th style={{textAlign: 'center'}}>Actions</th>
											</tr>
										</thead>
										<tbody>
											{(this.state.examinations.length > 0) ? (
												renderExaminations
											) : (
												<tr>
													<td colSpan="6"><i>No candidate has been invited.</i></td>
												</tr>
											)}
										</tbody>
									</table>
								</div>

								<div role="tabpanel" className="tab-pane" id="purchases">
									<table className="table" style={{marginTop: 10}}>
										<thead>
											<tr>
												<th>#</th>
												<th>Date</th>
												<th>Total Credit</th>
												<th>Total Amount</th>
											</tr>
										</thead>
										<tbody>
											{(this.state.purchaseHistories.length > 0) ? (
												renderPurchases
											) : (
												<tr>
													<td colSpan="6"><i>No purchases has been made.</i></td>
												</tr>
											)}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				) : null}
			</div>
		);
	}
}

module.exports = Radium(AccountProductDetail);
