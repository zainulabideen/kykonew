import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import async from 'async';

import ServerApiHelper from '../helpers/server-api-helper';

const styles = {
	container: {
		marginTop: 10
	},
};

class AccountRegistration extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			name: '',
			email: '',
			address: '',
			type: 'corporate'
		};
	}

	onChangeInput(name, event) {
		var state = this.state;

		switch (name) {
			case 'username':
			case 'email':
				state[name] = event.target.value.trim();
				break;
			default:
				state[name] = event.target.value;
		}
		
		this.setState(state);
	}

	onClickRegister() {
		var data = this.state;

		if (data.username.length > 0 && data.name.length > 0 && data.email.length > 0 && data.address.length > 0) {
			$('#btnRegister').button('loading');

			ServerApiHelper.register(data, function (error) {
				$('#btnRegister').button('reset');

				if (!error) {
					alert('Succesfully registered. Please check your email for account password change.');
					window.location.href = '#/accounts/login';
				} else {
					alert(error);
				}
			});
		} else {
			alert('Please fill in all require information.');
		}
	}

	render() {
		return (
			<div style={styles.container} className="container">
				<div className="row">
					<div className="col-md-offset-3 col-md-6">
						<div className="form-group">
							<label>Username</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="Username" 
								value={this.state.username} 
								onChange={this.onChangeInput.bind(this, 'username')}
							/>
						</div>
						<div className="form-group">
							<label>Name</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="Name" 
								value={this.state.name} 
								onChange={this.onChangeInput.bind(this, 'name')}
							/>
						</div>
						<div className="form-group">
							<label>Email</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="Email" 
								value={this.state.email} 
								onChange={this.onChangeInput.bind(this, 'email')}
							/>
						</div>
						<div className="form-group">
							<label>Address</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="Address" 
								value={this.state.address} 
								onChange={this.onChangeInput.bind(this, 'address')}
							/>
						</div>
						<div className="form-group">
							<label>Type</label>
							<select 
								className="form-control" 
								value={this.state.type} 
								onChange={this.onChangeInput.bind(this, 'type')}
							>
								<option value="personal">Personal</option>
								<option value="corporate">Corporate</option>
								<option value="education">Education</option>
							</select>
						</div>
						<div style={{textAlign: 'right'}}>
							<button id="btnRegister" className="btn btn-block btn-primary" onClick={this.onClickRegister.bind(this)}>Register</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(AccountRegistration);