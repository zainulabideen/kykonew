import React from 'react';
import ReactDOM from 'react-dom';

import {
  BrowserRouter as Router,
  HashRouter,
  Route,
  Redirect
} from 'react-router-dom';

import ProductExamination from './components/product-examination';
import AccountLogin from './components/account-login';
import AccountRegistration from './components/account-registration';
import AccountProductList from './components/account-product-list';
import AccountProductDetail from './components/account-product-detail';

ReactDOM.render((
	<HashRouter>
		<div>
			<Route exact path="/" render={() => (<Redirect to="/accounts/login" />)} />
			<Route exact path="/products/:productId/examinations/:examinationId" component={ProductExamination} />
			<Route exact path="/accounts/registration" component={AccountRegistration} />
			<Route exact path="/accounts/login" component={AccountLogin} />
			<Route exact path="/accounts/:accountToken/products" component={AccountProductList} />
			<Route exact path="/accounts/:accountToken/products/:productId" component={AccountProductDetail} />
		</div>
	</HashRouter>
), document.getElementById('app'));