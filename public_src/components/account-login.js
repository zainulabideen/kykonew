import React from 'react';
import Radium from 'radium';
import _ from 'underscore';
import async from 'async';

import ServerApiHelper from '../helpers/server-api-helper';

const styles = {
	container: {
		marginTop: 10
	},
};

class AccountLogin extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: ''
		};
	}

	onChangeInput(name, event) {
		var state = this.state;

		state[name] = event.target.value.trim();

		this.setState(state);
	}

	onClickLogin() {
		if (this.state.username.length > 0 && this.state.password.length > 0) {
			ServerApiHelper.login(this.state.username, this.state.password, function (error, data) {
				if (!error) {
					var loginToken = require('shortid').generate();
					sessionStorage.setItem(loginToken, data._id);
					window.location.href = '#/accounts/' + loginToken + '/products';
				} else {
					alert(error);
				}
			});
		} else {
			alert('Username and password are require.');
		}
	}

	onClickForgotPassword() {
		if (this.state.username.length > 0) {
			ServerApiHelper.passwordReset(this.state.username, function (error) {
				if (!error) {
					alert('Please check your email for password change.');
				} else {
					alert(error);
				}
			});
		} else {
			alert('Username is require.');
		}
	}

	onClickRegister() {
		window.location.href = '#/accounts/registration';
	}

	render() {
		return (
			<div style={styles.container} className="container">
				<div className="row">
					<div className="col-md-offset-4 col-md-4">
						<div className="form-group">
							<input 
								type="text" 
								className="form-control" 
								placeholder="Username" 
								value={this.state.username} 
								onChange={this.onChangeInput.bind(this, 'username')}
							/>
						</div>
						<div className="form-group">
							<input 
								type="password" 
								className="form-control" 
								placeholder="Password" 
								value={this.state.password} 
								onChange={this.onChangeInput.bind(this, 'password')}
							/>
						</div>
						<div>
							<button className="btn btn-block btn-primary" onClick={this.onClickLogin.bind(this)}>Login</button>
						</div>
						<div style={{marginTop: 5}}>
							<div className="col-md-6" style={{paddingLeft: 0, textAlign: 'left'}}>
								<button className="btn btn-link" style={{padding: 0}} onClick={this.onClickForgotPassword.bind(this)}>Forgot Password</button>
							</div>
							<div className="col-md-6" style={{paddingRight: 0, textAlign: 'right'}}>
								<button className="btn btn-link" style={{padding: 0}} onClick={this.onClickRegister.bind(this)}>Register</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Radium(AccountLogin);